//$Id$

/*
  ATLAS ROS Software

  Class: VMEReadoutModuleUser
  Author: J.O.Petersen CERN 	
*/

#ifndef VMEMODULEUSER_H
#define VMEMODULEUSER_H

#include <sys/types.h>
#include "oh/OHRawProvider.h"
#include "ROSInterruptScheduler/InterruptHandler.h"
#include "ROSCore/ScheduledUserAction.h"
#include "ROSInterruptScheduler/VMEInterruptDescriptor.h"
#include "ROSCore/SequentialDataChannel.h"
#include "ROSCore/ReadoutModule.h"

namespace ROS 
{
  class VMEReadoutModuleUser : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);

//    virtual ISInfo *getISInfo(void);
    virtual void clearInfo();
    virtual void publish(void);
    virtual void publishFullStats(void);
    virtual int getUserCommandStatistics(std::string command, u_char* buffer, u_int maxSize, u_long physicalAddress);
    VMEReadoutModuleUser();
    virtual ~VMEReadoutModuleUser() noexcept;

    virtual const std::vector<DataChannel *> *channels();

    static const u_int c_vmeBaseCorbo = 0x700000;   // CORBO address hardwired ..

  private:
    DFCountedPointer<Config> m_configuration;       //A configuration object
    bool m_usePollingModule;                        //A module parameter: Defines if polling of interrupts are used to detect new fragments
    int m_errorVectorModule;                        //A module parameter: If != 0 defines the VME interrupt vector used by the module to signal error 
    int m_deltaTimeMs;                              //A module parameter: If >= 0 two (!) scheduled user actions will be installed
    vector <u_int> m_id;                            //A channel parameter: The logical ID
    vector <u_int> m_rolPhysicalAddress;            //A channel parameter: The physical address of the data channel
    vector <bool> m_usePolling;                     //A channel parameter: The polling flag (initialized from m_usePollingModule)
    vector <int> m_corboChannel;                    //A channel parameter: The number of the Corbo channel used by this data channel
    bool m_blockTransferEnabled;                    //Switch between DMA and single cycles for the read-out of the data channel
    vector <u_int> m_vmeAddress;                    //A channel parameter: The VMEbus base address at which the data channel provides access to the data
    vector <u_int> m_vmeWindowSize;                 //A channel parameter: The size (in bytes) of the slave window
    vector <u_int> m_vmeAddressModifier;            //A channel parameter: The AM code for access to the data channel (encoded as required by VME_MasterMap
    vector <int> m_vmeScHandle;                     //A channel parameter: The handle returned by VME_MasterMap for single cycle access
    vector <u_long> m_vmeVirtualPtr;                //A channel parameter: The virtual address of m_vmeScHandle  for fast single cycle access
    vector <VMEInterruptDescriptor> m_vmeInterrupt; //A channel parameter: An object that represents the "data available" interrupt of a data channel

    int m_numberOfDataChannels;                     // The number of data channel of the module
    vector<DataChannel *> m_dataChannels;	    // a container for the (generic) DataChannels
    InterruptHandler *m_handler;		    // An object (of type ROD0DataInterruptHandler). It represents the handler for the "data is available" interrupt
    InterruptHandler *m_errorHandler;               // An object (of type ROD0ErrorInterruptHandler). It represents the handler for the "error detected" interrupt
    ScheduledUserAction *m_scheduledUserAction0;    // An object (of type ROD0ScheduledUserAction). It represents the scheduled user action
    OHRawProvider<> *m_rawOH;                       // A link to the histogramming system
  };

  inline const std::vector<DataChannel *> * VMEReadoutModuleUser::channels()
  {
    return &m_dataChannels;
  }
}
#endif // VMEMODULEUSER_H
