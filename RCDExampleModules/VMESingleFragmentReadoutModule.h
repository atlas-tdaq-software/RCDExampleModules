//$Id$

/*
  ATLAS ROS Software

  Class: VMESingleFragmentReadoutModule
  Author: J.O.Petersen CERN 	
*/

#ifndef VMESINGLEFRAGMENTMODULE_H
#define VMESINGLEFRAGMENTMODULE_H

#include <sys/types.h>
#include "ROSCore/ReadoutModule.h"

namespace ROS
{
  class VMESingleFragmentReadoutModule : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    VMESingleFragmentReadoutModule();
    virtual ~VMESingleFragmentReadoutModule() noexcept;
    virtual const std::vector<DataChannel *> *channels();

  private:  
    DFCountedPointer<Config> m_configuration;        //A pointer to a configuration object
    std::vector <u_int> m_id;                        //The logical ID of a channel
    std::vector <u_int> m_rolPhysicalAddress;        //Used to compute the VMEbus base address of a channel (within the VMEbus memory module). Not a very elegant implementation 
    std::vector <u_int> m_vmeAddress;                //The VMEbus base address of a data channel
    std::vector <u_int> m_vmeWindowSize;             //The size of the VMEbus window of a data channel
    std::vector <u_int> m_vmeAddressModifier;        //The AM code for access to the data channel
    std::vector <int> m_vmeScHandle;                 //The return value of VME_MasterMap
    std::vector <u_long> m_vmeVirtualPtr;            //A virtual address pointing to the VMEbus base address of a data channel

    int m_numberOfDataChannels;                      //The total number of data channels managed by this module
    std::vector<DataChannel *> m_dataChannels;       //An array of generic DataChannels
  };

  inline const std::vector<DataChannel *> * VMESingleFragmentReadoutModule::channels()
  {
    return &m_dataChannels;
  }
}
#endif // VMESINGLEFRAGMENTMODULE_H
