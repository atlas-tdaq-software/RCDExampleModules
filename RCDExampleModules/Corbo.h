//$Id$

/*

Some CORBO constants

*/

#ifndef MYCORBO_H
#define MYCORBO_H

  enum 
  {
    // (internal) busy enabled, busy latch, front panel input, internal busy out, count triggers, disable external counter clear, pushbutton resets BIM
    INI_CSR1 = 0xc0,
    // (internal) busy disabled, busy latch, front panel input, internal busy out, count triggers, disable external counter clear, pushbutton resets BIM
    DIS_CSR1 = 0xc1,
    // interrupt enable (IRE), autoclear, level 0
    INI_BIMCR0 = 0x18,
    // interrupt disable (level 0)
    DIS_BIMCR0 = 0x00
  };

#endif // MYCORBO_H
