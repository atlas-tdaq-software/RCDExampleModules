// $Id$

/*
  ATLAS ROS Software

  Class: ROD0ErrorInterruptHandler
  Authors: B.Gorini, M.Joos, J.Petersen
*/

#ifndef ROD0ERRORINTERRUPTHANDLER_H
#define ROD0ERRORINTERRUPTHANDLER_H

#include "ROSInterruptScheduler/InterruptHandler.h"


namespace ROS 
{
  class ROD0ErrorInterruptHandler : public InterruptHandler 
  {
  public:
    ROD0ErrorInterruptHandler(VMEInterruptDescriptor vmeInt);
    ROD0ErrorInterruptHandler(std::vector<VMEInterruptDescriptor> vmeInterrupts);
    virtual ~ROD0ErrorInterruptHandler();
    virtual void reactTo(VMEInterruptDescriptor vmeInt);
    virtual void prepareForRun(void);
    virtual void stopDC(void);

  private:
    int m_corboChannel;      //The corbo channel used for "error" interrupts
    int m_interruptLevel;    //The interrupt level of the Corbo
    bool m_stopTriggerFlag;  //Enables interrupts when we are ready to receive them 
  };
}
#endif //ROD0ERRORINTERRUPTHANDLER_H
