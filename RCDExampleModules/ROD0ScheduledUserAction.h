// $Id$

/*
  ATLAS ROS Software

  Class: ROD0ScheduledUserAction
  Authors: B.Gorini, M.Joos, J.Petersen
*/

#ifndef ROD0SCHEDULEDUSERACTION_H
#define ROD0SCHEDULEDUSERACTION_H

#include <vector>
#include "oh/OHRawProvider.h"
#include "ROSCore/ScheduledUserAction.h"


namespace ROS 
{
  class ROD0ScheduledUserAction : public ScheduledUserAction 
  {
  public:
    ROD0ScheduledUserAction(int deltaTimeMs);
    virtual ~ROD0ScheduledUserAction();
    virtual void reactTo(void);
    void prepareForRun(void);
    void stopDC(void); 
    void printTimeHistogram(void);
    void publishTimeHistogram(OHRawProvider<>* provider);
    void clearInfo(void);

  private:
    bool m_stopTriggerFlag;	       
    int m_deltaTimeMs;             
    int m_low;	                       
    int *m_hist;	               
    float *m_errors;	               
    static const int m_bins = 12;	// 10 + underflow + overflow
    static const int m_width = 20;	// milliseconds
  };
}
#endif //ROD0SCHEDULEDUSERACTION_H
