// -*- c++ -*-

/*
  ATLAS RCD Software

  Class: RCD Example Readout Module
  Authors: ATLAS RCD group 	
*/

#ifndef EXAMPLEFEBMODULE_H
#define EXAMPLEFEBMODULE_H

#include <string>
#include <vector>

#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/Config.h"

namespace RCD
{
  /**
   * \brief Sample interface to a dummy module controlled by 
   * the IOManager.
   *
   * This is an example implementation class of the
   * ReadOutModule class,
   * describing a generic dummy module in the RCD system.
   * The ReadOutModule base class extends the Controllable class, that has a 
   * virtual method for each FSM state transition.
   * Just for clarity, in this example we overload all the
   * available methods.
   * In this case, the module corresponds to a number(0...n)
   * of data channels from which data have to be collected
   * to undergo ROD emulation. In general, if RCD is used to 
   * provide ROD emulation reading out VME modules, 
   * it is advisable to use one module per data channel.
   * This class has been used to test the RCDEmulatorRequest class.
   */

  using namespace ROS;

  class ExampleFEBModule : public ReadoutModule
  {
   public:

    ExampleFEBModule();
    virtual ~ExampleFEBModule() noexcept;

    /**
     * Set/update the values of the class internal variables
     * Called after the creator to initialize the class.
     * @param configuration contains the configuration parameters according
     *        to the class structure in the database schema
     */
    virtual void setup(DFCountedPointer<Config> configuration);
    
    virtual void clearInfo();                               //Reset internal statistics
    virtual DFCountedPointer<Config> getInfo();             //Get performance statistics
    virtual const std::vector<DataChannel *> *channels();   //Get the list of channels connected to this module

    /**
     * Inherited from the IOMPlugin class. This methods
     * are called at state transitions. In general, only
     * needed methods have to be overloaded.
     * State transitions not related to RCD are omitted from this
     * list.
     */
    virtual void configure(const daq::rc::TransitionCmd&) {};
    virtual void connect(const daq::rc::TransitionCmd&) {};
    virtual void prepareForRun(const daq::rc::TransitionCmd&) {};
    virtual void stopDC(const daq::rc::TransitionCmd&) {};
    virtual void probe() {};
    virtual void disconnect(const daq::rc::TransitionCmd&) {};
    virtual void unconfigure(const daq::rc::TransitionCmd&) {};
    virtual void checkpoint() {};
    virtual void userCommand(std::string &commandName, std::string &argument) {};

   private:
    std::vector<DataChannel *> m_dataChannels;  //Global vector of physical addresses of emulated links

    /* These variables are related to this specific implementation */
    DFCountedPointer<Config> m_modInfo;
    int   m_modnum;
    int   m_maxFragmentSize;
    u_int m_fragments;
    float m_averageLength;
    float m_fEmpty;
  };

  inline const std::vector<DataChannel *> *ExampleFEBModule::channels()
  {
    return &m_dataChannels;
  }
}
#endif
