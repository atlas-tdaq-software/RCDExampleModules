// -*- c++ -*- $Id$

/*
  ATLAS RCD/ROS Software

  Class: RCDMODULESEXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef RCDMODULESEXCEPTION_H
#define RCDMODULESEXCEPTION_H

#include <sys/types.h>
#include <string>
#include <iostream>
#include "DFExceptions/ROSException.h"

namespace RCD 
{
  using namespace ROS;

  class RCDModulesException : public ROSException 
  {
  public:
    enum ErrorCode 
    {
      CORBO_OPEN = 100,
      CORBO_CLOSE,
      ERROR_INTERRUPT,
      BT_STATUS
    };
    RCDModulesException(ErrorCode error, std::string description);
    RCDModulesException(ErrorCode error);
    RCDModulesException(ErrorCode error, std::string description, const ers::Context& context);
    RCDModulesException(ErrorCode error, const ers::Context& context);
    RCDModulesException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);
    virtual ~RCDModulesException() throw() { }

  protected:
    virtual std::string getErrorString(u_int errorId) const;
  };


  inline RCDModulesException::RCDModulesException(RCDModulesException::ErrorCode error, std::string description) 
    : ROSException("RCDExampleModules", error, getErrorString(error), description) { }
  
  inline RCDModulesException::RCDModulesException(RCDModulesException::ErrorCode error) 
    : ROSException("RCDExampleModules", error, getErrorString(error)) { }

  inline RCDModulesException::RCDModulesException(RCDModulesException::ErrorCode error, std::string description, const ers::Context& context) 
    : ROSException("RCDExampleModules", error, getErrorString(error), description,context) { }
  
  inline RCDModulesException::RCDModulesException(RCDModulesException::ErrorCode error, const ers::Context& context) 
    : ROSException("RCDExampleModules", error, getErrorString(error), context) { }

  inline RCDModulesException::RCDModulesException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context)
    : ROSException(cause, "RCDExampleModules", error, getErrorString(error), description, context) { }

  inline std::string RCDModulesException::getErrorString(u_int errorId) const 
  {
    std::string rc;
    switch (errorId) 
    {
    case CORBO_OPEN:
      rc = "Error on CORBO open";
      break;
    case CORBO_CLOSE:
      rc = "Error on CORBO close";
      break;
    case ERROR_INTERRUPT:
      rc = "Error interrupt occured";
      break;
    case BT_STATUS:
      rc = "Error on block transfer. VME_BlockTransferStatus: ";
      break;
    default:
      rc = "";
      break;
    }
    return rc;
  }
}
#endif //RCDMODULESEXCEPTION_H
