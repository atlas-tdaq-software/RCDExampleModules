// $Id$

/*
  ATLAS ROS Software

  Class: ROD0DataChannel
  Authors: B.Gorini, M.Joos, J.Petersen

*/

#ifndef ROD0DATACHANNEL_H
#define ROD0DATACHANNEL_H

#include <sys/types.h>
#include "ROSInfo/RODDataChannelInfo.h"
#include "ROSCore/SequentialDataChannel.h"
#include "ROSInterruptScheduler/VMEInterruptDescriptor.h"


namespace RCD 
{
  using namespace ROS;

  class ROD0DataChannel : public SequentialDataChannel 
  {
  public:
    ROD0DataChannel(u_int id, 
                    u_int configId, 
		    u_int rolPhysicalAddress,
		    bool usePolling,
		    DFCountedPointer<Config> configuration,
		    u_int vmeAddress,
                    u_long vmeBaseVA,
                    VMEInterruptDescriptor vmeInterrupt,
		    int corboChannel,
		    bool blockTransferEnabled,
		    RODDataChannelInfo *info = new RODDataChannelInfo());  // default);
   
    virtual ~ROD0DataChannel();
    virtual int getNextFragment(u_int *buffer, int max_size, u_long pciAddress = 0);
    virtual int poll(void);
    void resetL1id(void);
    void prepareForRun(void);
    void stopDC(void);
    virtual void clearInfo();
    virtual ISInfo *getISInfo(void);
    
  private:
    u_long m_vmeBaseVA;               //Virtual VMEbus base address for data read-out by SC access
    u_int m_vmeAddress;               //Physical VMEbus base address for data read-out by DMA access
    int m_corboChannel;               //Number of the associated channel of the Corbo
    int m_interruptLevel;             //Interrupt level for "data ready" interrupts
    bool m_usePolling;                //Select polling or interrupt based detection method for new data
    bool m_blockTransferEnabled;      //Select data read-out via DMA
    int m_l1id_hack;                  //Needed to generated the L1ID. VME_ROD_write does not do it for us
    bool m_stopTriggerFlag;           //Unblocks the Corbo interrupts when the trigger is enabled
    RODDataChannelInfo *m_statistics; //For IS
  };
}
#endif //ROD0DATACHANNEL_H
