// $Id$

/*
  ATLAS ROS Software

  Class: ROD0DataInterruptHandler
  Authors: B.Gorini, M.Joos, J.Petersen
*/

#ifndef ROD0DATAINTERRUPTHANDLER_H
#define ROD0DATAINTERRUPTHANDLER_H

#include "ROSInterruptScheduler/InterruptHandler.h"
#include "ROSCore/SequentialDataChannel.h"


namespace ROS 
{
  class ROD0DataInterruptHandler : public InterruptHandler 
  {
  public:
    ROD0DataInterruptHandler(VMEInterruptDescriptor vmeInt, SequentialDataChannel *channel);
    ROD0DataInterruptHandler(std::vector<VMEInterruptDescriptor> vmeInterrupts, std::vector<SequentialDataChannel *> channels);
    virtual ~ROD0DataInterruptHandler();
    virtual void reactTo(VMEInterruptDescriptor vmeInt);

  private:
    std::map <int, SequentialDataChannel *> m_dataChannelsMap;    // to map vectors onto channels
  };
}
#endif //ROD0DATAINTERRUPTHANDLER_H
