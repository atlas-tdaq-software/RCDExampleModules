//$Id$

/*
  ATLAS RCD Software

  Class: ExampleFEBDataChannel
  Authors: ATLAS RCD group 	
*/


#ifndef EXAMPLEFEBDATACHANNEL_H
#define EXAMPLEFEBDATACHANNEL_H

#include "ROSCore/DataChannel.h"
#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFFastMutex.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "DFSubSystemItem/Config.h"

namespace RCD 
{
  /**
   * \brief Interface to a dummy module input data channel.
   *
   * This is an example implementation class of the
   * DataChannel class,
   * describing a generic input data channel
   * for a dummy module in the RCD system.
   * It inherits, in addition to the ones implemented here,
   * all the public methods for data access in the base class.
   */
  
  using namespace ROS;

  class ExampleFEBDataChannel : public DataChannel
  {
   public:

    /**
     * The ExampleFEBDataChannel class is created during
     * ExampleFEBModule setup.
     * @param channel_id data channel identifier
     * @param config_id data channel "config" identifier i.e. an internal sequential channel NUMBER
     * @param physical_address physical address of the module
     * @param configuration contains the configuration parameters according
     *        to the class structure in the database schema
     */
    ExampleFEBDataChannel(u_int channel_id, 
			  u_int config_id,
			  u_long physical_address,
			  DFCountedPointer<Config> configuration);

    virtual ~ExampleFEBDataChannel();
    
    /**
     * Get an event fragment
     * @param ticket the ticket returned by the preceding requestFragment call
     */
    virtual EventFragment *getFragment(int ticket);

    /**
     * Request a Fragment.  It returns a ticket that has to be
     * passed to the getFragment method to obtain the requested fragment.
     * @param nrequested The "request for an event fragment" number.
     * In case of ROD emulation, 32bit Level 1 
     * event identifier of fragment to be retrieved. In case of
     * ROD monitoring, it simply represents the request number.
     */
    virtual int requestFragment(int nrequested);

    virtual void clearInfo();                      // Reset internal statistics 
    virtual DFCountedPointer<Config> getInfo();    // Get performance statistics 

    // Not relevant for Rod Crate DAQ
    void releaseFragment(const std::vector < u_int > * level1Ids) {};
    void releaseFragment(int level1Id) {};

  private:
    DFCountedPointer<Config>    m_outinfo;
    MemoryPool                  *m_memoryPool;
    DFFastMutex                 *m_mutex;
    int                         m_maxFragmentSize;
    u_int                       m_fragments;
    float                       m_averageLength;
    float                       m_fEmpty;
  };
}
#endif
