//$Id$

/*
  ATLAS ROS Software

  Class: EmptyReadoutModule
  Author: Sonia Gameiro CERN 	
*/

#ifndef EMPTYREADOUTMODULE_H
#define EMPTYREADOUTMODULE_H

#include "ROSCore/ReadoutModule.h"

namespace ROS 
{
  class EmptyReadoutModule : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);
    virtual void unconfigureconst daq::rc::TransitionCmd&();
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer < Config > getInfo();
    virtual void probe(void);
    virtual void publishFullStatistics(void);
    EmptyReadoutModule();
    virtual ~EmptyReadoutModule() noexcept;

    virtual const std::vector<DataChannel *> * channels();
    
  private:

    DFCountedPointer<Config> m_configuration;

    vector<DataChannel *> m_dataChannels;
  };

  inline const std::vector<DataChannel *> * EmptyReadoutModule::channels()
  {
    return &m_dataChannels;
  }  
}
#endif // EMPTYREADOUTMODULE_H
