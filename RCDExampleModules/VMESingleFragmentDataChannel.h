// $Id$

/*
  ATLAS ROS Software

  Class: VMESingleFragmentDataChannel
  Authors: G. Crone, S. Gameiro, B.Gorini, J.Petersen
*/

#ifndef VMESINGLEFRAGMENTDATACHANNEL_H
#define VMESINGLEFRAGMENTDATACHANNEL_H

#include <sys/types.h>
#include "ROSInfo/VMESingleFragmentDataChannelInfo.h"
#include "ROSCore/SingleFragmentDataChannel.h"

namespace ROS 
{
  class VMESingleFragmentDataChannel : public SingleFragmentDataChannel 
  {
  public:
    VMESingleFragmentDataChannel(u_int id, 
    				 u_int configId, 
				 u_int rolPhysicalAddress,
				 u_long vmeBaseVA, 
				 DFCountedPointer<Config> configuration,
				 VMESingleFragmentDataChannelInfo* = new VMESingleFragmentDataChannelInfo());
    
    virtual ~VMESingleFragmentDataChannel();
    virtual int getNextFragment(u_int *buffer, int max_size, u_int *status, u_long pciAddress = 0);
    void clearInfo(void);
    virtual ISInfo *getISInfo(void);

  private:
    u_long m_vmeBaseVA;                              //the virtual address pointing to the VMEbus base address of the data channel 
    VMESingleFragmentDataChannelInfo *m_statistics;  //for IS
  };
}
#endif //VMESINGLEFRAGMENTDATACHANNEL_H
