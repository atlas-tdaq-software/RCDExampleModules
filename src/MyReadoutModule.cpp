//$Id$
/************************************************************************/
/*									*/
/* File             : MyReadoutModule.cpp				*/
/*									*/
/* Authors          : S. Gameiro, J.Petersen CERN			*/
/*									*/
/* This file is a template for the RCD users who wish to develop the	*/ 
/* code of their Readout Modules.				  	*/
/*									*/
/* IT IS NOT MEANT TO COMPILE. 						*/
/*									*/
/* It is based on a ficticious schema that defines the class 		*/
/* MyReadoutModule, containing:						*/
/* 	- several attributes (VMEaddr, InputFragmentType, RODstatus...)	*/
/* 	- one relationship with cardinality 1 to a MyRelationshipClass1 */
/*		class, and relationship name MyRelationshipName1.	*/
/*	- one relationship with cardinality "many" to a 		*/
/*		MyRelationshipClass2 class, and relationship name	*/
/*		MyRelationshipName2.					*/
/*									*/
/********* C 2005 - ROS/RCD  ********************************************/

#include <unistd.h>
#include <iostream>

#include "RCDTemplate/MyReadoutModule.h"
#include "RCDTemplate/MyDataInterruptHandler.h"
#include "RCDTemplate/MyErrorInterruptHandler.h"
#include "RCDTemplate/MyScheduledUserAction.h"
#include "RCDTemplate/MyDataChannel.h"

// Includes error definitions
#include "RCDTemplate/RCDModulesException.h"

// Includes the definition of tracing and debugging messages (DEBUG_TEXT, etc...)
#include "DFDebug/DFDebug.h"

#include "ROSUtilities/ROSErrorReporting.h"

// In order to parse the relationships of your module via the pointer to the global 
// configuration database:
// you need the dal of your own module
#include "Mydal/MyReadoutModule.h"
// and you need the dal of the relationships your module is pointing at
#include "Xdal/MyRelationshipClass1.h"
#include "Ydal/MyRelationshipClass2.h"

// To handle the config exception in the "try-catch" instructions
#include "DFSubSystemItem/ConfigException.h"

// Variable to be used for tracing and debugging. You should create a string on your own,
// (RCDLARG, RCDPIXEL...) and make it match a value that corresponds to your detector,
// following the conventions pointed out at
// /afs/cern.ch/atlas/project/tdaq/cmt/"release_name"/DFDebug/v2r0p0/doc/DFDebug.pdf

enum 
{
  DFDB_RCDDETECTOR = 30000	
};


using namespace ROS;
using namespace RCD;


/********************************/
MyReadoutModule::MyReadoutModule() 
/********************************/
{ 
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::constructor: Entered");
}


/*********************************/
MyReadoutModule::~MyReadoutModule() 
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::destructor: Entered");

  // Delete the data channels that you created, if you have some.

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		
  }

  // delete the User "Data" Interrupt Handler
  if (m_handler != 0) 
    delete m_handler;

  // delete the User Error Interrupt Handler
  if (m_errorHandler != 0) 
    delete m_errorHandler;

  // delete the User Timer Poll Handler
  if (m_scheduledUserAction != 0) 
    delete m_scheduledUserAction;
}


/*****************************************************************/
void MyReadoutModule::setup(DFCountedPointer<Config> configuration) 
/*****************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::setup: Entered");

  // In this method you should extract the attributes and relationships from the configuration
  // object, which contains the parameters you defined in your schema.

  // Extracting attributes:
  
  u_int vmebus_address = configuration->getUInt("VMEaddr");	//how to extract a u32
  DEBUG_TEXT(DFDB_RCDDETECTOR, 20, "VMEbus address = " << HEX(vmebus_address));
  
  string fragmentType = configuration->getString("InputFragmentType");	//how to extract an enum type
  DEBUG_TEXT(DFDB_RCDDETECTOR, 20, "Input Fragment Type = " << fragmentType);
  
  if (fragmentType == "RODFragment") 
  {
    // something
  } 
  else if (fragmentType == "ROSFragment") 
  {
    // something
  }  	  
  // etc, etc...
  
  bool rodStatus = configuration->getBool("RODstatus");  // how to extract a bool
  DEBUG_TEXT(DFDB_RCDDETECTOR, 20, " Status of this ROD, enabled or disabled = " << rodStatus);
  
  //Extracting relationships:
  
  Configuration* confDB;
  
  try 
  {
    confDB = configuration->getPointer<Configuration>("configurationDB");
  }

  catch (ConfigException& except) 
  {
    DEBUG_TEXT(DFDB_RCDDETECTOR, 20, "MyReadoutModule::setup: failed to get pointer to database");
    confDB = 0;
  }

  SEND_ROS_INFO("Configuration pointer = " << confDB);

  if (confDB != 0) 
  { 		
    // database not found OR Ascii Config File
    // (where configurationDB = 0)
    // gives a pointer to this Readout Module's database entry

    const Mydal::MyReadoutModule* dbModule = confDB->get<Mydal::MyReadoutModule>(configuration->getString("UID"));
 
    // Reading a relationship which has cardinality 1
 
    const Xdal::MyRelationshipClass1* rel1 = dbModule->get_MyRelationshipName1();
    string rel1Attr = rel1->get_relationship1Attribute();					
    SEND_ROS_INFO("This is the value of one of the attributes of relationship 1: " << rel1Attr);
       
    // Reading a relationship which has cardinality "many"
    
    for(std::vector<const Ydal::MyRelationshipClass2*>::const_iterator iter = dbModule->get_MyRelationshipName2().begin();
      	iter != dbModule->get_MyRelationshipName2().end(); ++iter) 
    {
      Ydal::MyRelationshipClass2* rel2 = const_cast<Ydal::MyRelationshipClass2*>(*iter);
      u_int rel2Attr = rel2->get_relationship2Attribute();					
      SEND_ROS_INFO("This is the value of one of the attributes of relationship 2: " << rel2Attr);
    }
  }

  // The Data Channels cannot have parameters on their own.
  // To define parameters that are channel specific, you should "extrapolate" them
  // from the readout module ones. This is very implementation-specific. For example:

  u_int vmeModuleAddress = configuration->getUInt("VMEbusAddress");
  for (int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_rolPhysicalAddress[chan] = configuration->getInt("ROLPhysicalAddress",chan);
    m_vmeAddress[chan] = vmeModuleAddress + m_rolPhysicalAddress[chan]*0x800;
  }
}


/******************************/
void MyReadoutModule::load(void)
/******************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::load: Entered");

  // In this method you should 
  // 	- establish all the harware connections (VME_Open(), TM_Open()...) and configure them (VME_MasterMap(),...)
  //	- create the Data Channels
  // 	- create all the necessary handlers: DataInterruptHandler, ErrorInterruptHandler, ScheduledUserAction.

  // auxiliary vector for sequential channels
  std::vector<SequentialDataChannel *> seqChannels;

  //Create the DataChannel objects
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    // chan == the Config Id
    MyDataChannel *channel = new MyDataChannel();
    m_dataChannels.push_back(channel);
  }

  // handles the "data" interrupts coming from the module
  m_handler = 0;
  if (!m_usePollingModule) 
  {    
    m_handler = new MyDataInterruptHandler(m_vmeInterrupt, seqChannels);
    DEBUG_TEXT(DFDB_RCDDETECTOR, 5, "MyReadoutModule::load: Created MyDataInterruptHandler");
  }

  // handles the error interrupt from the module
  m_errorHandler = 0;
  if (m_errorVectorModule != 0) 
  {    
    VMEInterruptDescriptor errorInterrupt (m_errorVectorModule);
    m_errorHandler = new MyErrorInterruptHandler(errorInterrupt);
    DEBUG_TEXT(DFDB_RCDDETECTOR, 5, "MyReadoutModule::load: Created MyErrorInterruptHandler");
  }

  // handles the "time" interrupts
  m_scheduledUserAction0 = 0;
  if (m_deltaTimeMs != 0) 
  {
    m_scheduledUserAction0 = new MyScheduledUserAction(m_deltaTimeMs);
    DEBUG_TEXT(DFDB_RCDDETECTOR, 5, "MyReadoutModule::load: Created MyScheduledUserAction0");
  }

  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::load: Done");
}

/********************************/
void MyReadoutModule::unload(void)
/********************************/
{
  
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::unload: Entered");
  
  // Here you should "un-do" what you have done in the load() method, except for the 
  // deletion of the various Handlers, which is done in the detructor:
  // 	- Closing the hardware connections
  // 	- Unconfiguring it (VME_MasterUnmap...)
  
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::unload: Done");
}


/***********************************/
void MyReadoutModule::configure(void)
/***********************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 20, "MyReadoutModule::configure: Entered");
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::configure: Done");
}


/*************************************/
void MyReadoutModule::unconfigure(void)
/*************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::unconfigure: Entered");
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::unconfigure: Done");
}
    

/***************************************/    
void MyReadoutModule::prepareForRun(void)
/***************************************/    
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::prepareForRun: Entered");
}


/********************************/
void MyReadoutModule::stopFE(void)
/********************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::stopFE: Done");
}


/***************************/
void MyReadoutModule::probe()
/***************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::probe: Entered");

  // The probe() method will be called at a regular frequency (set in the ProbeInterval 
  // attribute of the RunControlApplication class in the database), usually a high one.
  // Tipically you would implement here the logic to publish to IS short status 
  // information.

  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::probe: Done");
}


/*******************************************/
void MyReadoutModule::publishFullStatistics()
/*******************************************/
{  
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::publishFullStatistics: Entered");
  
  // The publishFullStatistics() method will be called at a regular frequency 
  // (set in the FullStatisticsInterval attribute of the RunControlApplication 
  // class in the database), usually a slow one.
  // Tipically you would implement here the logic to publish big histograms that your
  // module may produce.

  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::publishFullStatistics: Done");
}    


/****************************************************/
DFCountedPointer < Config > MyReadoutModule::getInfo()
/****************************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::getInfo: Entered");

  DFCountedPointer<Config> configuration = Config::New();
  return(configuration);

}

/*******************************/
void MyReadoutModule::clearInfo()
/*******************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "MyReadoutModule::clearInfo  Function entered");
}

extern "C" 
{
  extern ReadoutModule* createMyReadoutModule();
}
ReadoutModule* createMyReadoutModule()
{
  return (new MyReadoutModule());
}
