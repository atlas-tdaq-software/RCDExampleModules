//$Id$
/************************************************************************/
/*									*/
/* File             : VMEReadoutModuleUser.cpp				*/
/*									*/
/* Authors          : B.Gorini, M.Joos, J.Petersen CERN			*/
/*									*/
/********* C 2011 - ROS/RCD  ********************************************/

#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include "rcc_error/rcc_error.h" 
#include "rcc_corbo/rcc_corbo.h" 
#include "vme_rcc/vme_rcc.h" 
#include "DFDebug/DFDebug.h"
#include "ROSModules/ModulesException.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "RCDExampleModules/RCDModulesException.h"
#include "RCDExampleModules/ROD0DataInterruptHandler.h"
#include "RCDExampleModules/ROD0ErrorInterruptHandler.h"
#include "RCDExampleModules/ROD0ScheduledUserAction.h"
#include "RCDExampleModules/ROD0DataChannel.h"
#include "RCDExampleModules/Corbo.h"
#include "RCDExampleModules/VMEReadoutModuleUser.h"


//Information about this example is in the ROS/RCD users guide


using namespace ROS;
using namespace RCD;


/******************************************/
VMEReadoutModuleUser::VMEReadoutModuleUser() 
/******************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::constructor: Entered");
  m_rawOH = 0;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::constructor: Done");
}


/*****************************************************/
VMEReadoutModuleUser::~VMEReadoutModuleUser() noexcept
/*****************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::destructor: Entered");

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		// we created them
  }

  // delete the User Interrupt Handler
  if (m_handler != 0) 
    delete m_handler;

  // delete the User Error Interrupt Handler
  if (m_errorHandler != 0) 
    delete m_errorHandler;

  // delete the User Timer Poll Handler
  if (m_scheduledUserAction0 != 0) 
    delete m_scheduledUserAction0;

  if (m_rawOH != 0) 
    delete m_rawOH;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::destructor: Done");
}


// setup gets parameters from configuration
/**********************************************************************/
void VMEReadoutModuleUser::setup(DFCountedPointer<Config> configuration) 
/**********************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::setup: Entered");

  m_configuration = configuration;

  m_numberOfDataChannels = configuration->getInt("numberOfChannels");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: m_numberOfDataChannels = " << m_numberOfDataChannels);

  m_blockTransferEnabled = configuration->getBool("BlockTransferEnabled");
 
  if (m_numberOfDataChannels != 0)
  {
    if (m_blockTransferEnabled) 
      {SEND_ROS_INFO("VMEReadoutModuleUser::setup: Block transfer enabled");}  //See: ROSUtilities/ROSErrorReporting.h
    else 
      {SEND_ROS_INFO("VMEReadoutModuleUser::setup: Single cycles enabled");}
  }
  
  // channel parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_id.push_back(0);
    m_rolPhysicalAddress.push_back(0);
    m_usePolling.push_back(0);
  }

  // VMEbus parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_vmeAddress.push_back(0);
    m_vmeWindowSize.push_back(0);
    m_vmeAddressModifier.push_back(0);
    m_vmeScHandle.push_back(0);
    m_vmeVirtualPtr.push_back(0);
    m_vmeInterrupt.push_back(0);
    m_corboChannel.push_back(0);
  }

  //get VMEbus parameters for the ROD MODULE
  u_int vmeModuleAddress         = configuration->getUInt("VMEbusAddress");
  u_int vmeModuleWindowSize      = configuration->getUInt("VMEbusWindowSize");
  u_int vmeModuleAddressModifier = configuration->getUInt("VMEbusAddressModifier");

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: Module VMEbus parameters : "
             << "  vmeAddress = 0x" << HEX(vmeModuleAddress)
             << "  vmeWindowSize  = 0x" << HEX(vmeModuleWindowSize)
             << "  vmeAddressModifier = 0x" << HEX(vmeModuleAddressModifier));

  int moduleInterrupt = configuration->getInt("VMEbusDataInterruptVector");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: MODULE interrupt vector = " << moduleInterrupt);

  m_errorVectorModule = configuration->getInt("VMEbusErrorInterruptVector");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: MODULE error interrupt  vector = " << m_errorVectorModule);

  m_deltaTimeMs = configuration->getInt("DeltaTimeScheduler");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: MODULE delta time (ms) = " << m_deltaTimeMs);

  int corboChannelModule = configuration->getInt("CorboChannelNumber");

  m_usePollingModule = configuration->getBool("UsePolling");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: m_usePollingModule = " << m_usePollingModule );

  for (int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_id[chan] = configuration->getInt("channelId", chan);
    m_rolPhysicalAddress[chan] = configuration->getInt("ROLPhysicalAddress", chan);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: chan = " << chan << "  id = " << m_id[chan]
             		       << " rolPhysicalAddress = " << m_rolPhysicalAddress[chan]);
    m_usePolling[chan] = m_usePollingModule;

    // the channel specific VMEbus parameters are derived from the Module parameters
    // via the physicalAddress - very application dependent
    m_corboChannel[chan] = corboChannelModule + m_rolPhysicalAddress[chan];   //Big risk for overflow! No check on database values

    m_vmeAddress[chan] = vmeModuleAddress + m_rolPhysicalAddress[chan] * 0x800;  //This has to match VME_ROD_write
    m_vmeWindowSize[chan] = vmeModuleWindowSize;
    m_vmeAddressModifier[chan] = vmeModuleAddressModifier;

    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: chan = " << chan
               << "  vmeAddress = 0x" << HEX(m_vmeAddress[chan])
               << "  vmeWindowSize  = 0x" << HEX(m_vmeWindowSize[chan])
               << "  vmeAddressModifier = 0x" << HEX(m_vmeAddressModifier[chan]));

    if (!m_usePollingModule) 
    {
      int ir = moduleInterrupt + m_rolPhysicalAddress[chan] * 1;    //Big risk for overflow!
      VMEInterruptDescriptor chanInterrupt(ir);                     //For info on VMEInterruptDescriptor have a look at the ROS/RCD users guide
      m_vmeInterrupt[chan] = chanInterrupt;
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: chan = " << chan
                 << "  vmeInteruptVector  = " << m_vmeInterrupt[chan].getVector()
                 << "  vmeInteruptLevel   = " << m_vmeInterrupt[chan].getLevel()
                 << "  vmeInteruptType    = " << m_vmeInterrupt[chan].getType());
    }
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::setup: Done");
}


/**********************************************************************/
void VMEReadoutModuleUser::configure(const daq::rc::TransitionCmd&)
/**********************************************************************/
{
  err_type ret;
  err_str rcc_err_str;
  int scHandle;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::configure: Entered");

  ret = VME_Open();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleInterrupt::configure: Error from VME_Open");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_OPEN, rcc_err_str);  //Preferably use a RCDModulesException
    throw ex1;
  }

  // CORBO
  ret = TM_open(c_vmeBaseCorbo);
  if(ret != CBE_OK && ret != CBE_ISOPEN) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleUser::configure: Error from TM_open");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, RCDModulesException, CORBO_OPEN, rcc_err_str);
    throw ex1;
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::configure: CORBO opened");

  // auxiliary vector for sequential channels
  std::vector<SequentialDataChannel *> seqChannels;

  //Create the DataChannel objects
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    // now the VMEbus mapping
    VME_MasterMap_t master_map;
    master_map.vmebus_address   = m_vmeAddress[chan];
    master_map.window_size      = m_vmeWindowSize[chan];
    master_map.address_modifier = m_vmeAddressModifier[chan];
    master_map.options          = 0x0;            

    ret = VME_MasterMap(&master_map, &scHandle);
    if (ret != VME_SUCCESS) 
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleUser::configure: Error from VME_MasterMap");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_MASTERMAP, rcc_err_str);
      throw ex1;
    }
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::configure: chan = " << chan << ", VME_MasterMap, handle = " << scHandle);

    m_vmeScHandle[chan] = scHandle;

    ret = VME_MasterMapVirtualLongAddress(scHandle, &m_vmeVirtualPtr[chan]);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::configure: chan = " << chan << ", VME_MasterMapVirtualAddress, VA = " << HEX(m_vmeVirtualPtr[chan]));

    // chan == the Config Id
    ROD0DataChannel *channel = new ROD0DataChannel(m_id[chan], 
                                                   chan, 
						   m_rolPhysicalAddress[chan],
                                                   m_usePolling[chan],
						   m_configuration,
                                                   m_vmeAddress[chan],
						   m_vmeVirtualPtr[chan],
                                                   m_vmeInterrupt[chan],
						   m_corboChannel[chan],
						   m_blockTransferEnabled);
    m_dataChannels.push_back(channel);

    // for the channel map in the user interrupt handler
    if (!m_usePollingModule) 
      seqChannels.push_back(channel);
  }

  // handles the "data" interrupts from ROD0
  m_handler = 0;
  if (!m_usePollingModule) 
  {    
    m_handler = new ROD0DataInterruptHandler(m_vmeInterrupt, seqChannels);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::configure: Created ROD0DataInterruptHandler");
  }

  // handles the error interrupts from ROD0
  m_errorHandler = 0;
  if (m_errorVectorModule != 0) 
  {    
    VMEInterruptDescriptor errorInterrupt(m_errorVectorModule);
    m_errorHandler = new ROD0ErrorInterruptHandler(errorInterrupt);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::configure: Created ROD0ErrorInterruptHandler");
  }

  // handles the "time" interrupts
  m_scheduledUserAction0 = 0;
  if (m_deltaTimeMs >= 0) 
  {
    m_scheduledUserAction0 = new ROD0ScheduledUserAction(m_deltaTimeMs);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::configure: Created ROD0ScheduledUserAction0");
    std::string partition = getenv("TDAQ_PARTITION");
    m_rawOH = new OHRawProvider<>(partition, "Histogramming", "VMEReadoutModuleUser");  //For background info contact Serg(u)ei / Gordon
  }    
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::configure: Done");
}


/************************************************************************/
void VMEReadoutModuleUser::unconfigure(const daq::rc::TransitionCmd&)
/************************************************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::unconfigure: Entered");

  // MasterUnmap the channels
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    ret = VME_MasterUnmap(m_vmeScHandle[chan]);
    if (ret != VME_SUCCESS) 
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleUser::unconfigure: Error from VME_MasterUnmap");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_MASTERUNMAP, rcc_err_str);
      throw ex1;
    }
  }

  ret = TM_close();
  if(ret != CBE_OK) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleUser::configure: Error from TM_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, RCDModulesException, CORBO_CLOSE, rcc_err_str);
    throw ex1;
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleUser::unconfigure: Error from VME_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_CLOSE, rcc_err_str);
    throw ex1;
  }

  //The rest of the job is done by the destructor for no obvious reason.....

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::unconfigure: Done");
}


/********************************************************************/
void VMEReadoutModuleUser::connect(const daq::rc::TransitionCmd&)
/********************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::connect: Entered (Empty function)");
}


/***********************************************************************/
void VMEReadoutModuleUser::disconnect(const daq::rc::TransitionCmd&)
/***********************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::disconnect: Entered (Empty function)");
}
    

/**************************************************************************/    
void VMEReadoutModuleUser::prepareForRun(const daq::rc::TransitionCmd&)
/**************************************************************************/    
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::prepareForRun: Entered");

  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<ROD0DataChannel *>(m_dataChannels[chan])->prepareForRun();

  if (m_errorVectorModule != 0) 
    dynamic_cast<ROD0ErrorInterruptHandler *>(m_errorHandler)->prepareForRun();

  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<ROD0DataChannel *>(m_dataChannels[chan])->resetL1id();

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::prepareForRun: Done");
}

/*******************************************************************/
void VMEReadoutModuleUser::stopDC(const daq::rc::TransitionCmd&)
/*******************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::stopDC: Entered");

  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<ROD0DataChannel *>(m_dataChannels[chan])->stopDC();

  if (m_errorVectorModule != 0) 
    dynamic_cast<ROD0ErrorInterruptHandler *>(m_errorHandler)->stopDC();

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::stopDC: Done");
}


/*************************************************************************/
int  VMEReadoutModuleUser::getUserCommandStatistics(std::string command,
                                                    u_char* buffer,
                                                    u_int /*maxSize*/,
                                                    u_long physicalAddress)
/*************************************************************************/
{
  int noRalf = 5;          //See Validation TWiki
  int noThilo = 10;
  int noMurrough = 15;

  DEBUG_TEXT(DFDB_ROSFM, 15, "VMEReadoutModuleUser::getUserCommandStatistics entered, command = " << command);
  DEBUG_TEXT(DFDB_ROSFM, 15, "VMEReadoutModuleUser::getUserCommandStatistics buffer =" << HEX((uintptr_t)buffer) << ", physicalAddress =" << physicalAddress);
  int used = 0;
  u_int* bufferUint = (u_int*) buffer;

  if (command == "Ralf") 
  {
    bufferUint[0] = 11111111;
    for (int i = 1; i < noRalf; i++) 
      bufferUint[i] = i + 10;
    used = noRalf * 4;  // bytes
  }
  if (command == "Thilo") 
  {
    bufferUint[0] = 22222222;
    for (int i = 1; i < noThilo; i++) 
      bufferUint[i] = i + 20;

    used = noThilo * 4;  // bytes
  }
  if (command == "Murrough") 
  {
    bufferUint[0] = 33333333;
    for (int i = 1; i < noMurrough; i++) 
      bufferUint[i] = i + 30;

    used = noMurrough * 4;  // bytes
  }
  std::cout << "VMEReadoutModuleUser::getUserCommandStatistics exit, command = " << command << "  used bytes = " << used << std::endl;
  return used;
}


/****************************************/
void VMEReadoutModuleUser::publish(void)
/****************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::publish: Entered");

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::publish: Done");
}

/****************************************************/
void VMEReadoutModuleUser::publishFullStats(void)
/****************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::publishFullStats: Entered");

  if (m_scheduledUserAction0 != 0) 
  {
    if (m_rawOH != 0) 
      dynamic_cast<ROD0ScheduledUserAction *>(m_scheduledUserAction0)->publishTimeHistogram(m_rawOH);
    else 
      dynamic_cast<ROD0ScheduledUserAction *>(m_scheduledUserAction0)->printTimeHistogram();
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::publishFullStats: Done");
}

// getISInfo requires a schema file ..

// called at prepareForRun
/************************************/
void VMEReadoutModuleUser::clearInfo()
/************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::clearInfo: Entered");

  if (m_scheduledUserAction0 !=0 ) 
    dynamic_cast<ROD0ScheduledUserAction *>(m_scheduledUserAction0)->clearInfo();
  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMEReadoutModuleUser::clearInfo: Done");
}

//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createVMEReadoutModuleUser();
}
ReadoutModule* createVMEReadoutModuleUser()
{
  return (new VMEReadoutModuleUser());
}
