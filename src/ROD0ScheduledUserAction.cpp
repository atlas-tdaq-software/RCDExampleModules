//$Id$
/************************************************************************/
/*									*/
/* File             : ROD0ScheduledUserAction.cpp			*/
/*									*/
/* Authors          : G. Crone, J.Petersen 				*/
/*									*/
/********* C 2008 - ROS/RCD *********************************************/


#include <iomanip>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "oh/OHRawProvider.h"
#include "RCDExampleModules/RCDModulesException.h"
#include "RCDExampleModules/ROD0ScheduledUserAction.h"


using namespace RCD;
using namespace ROS;


/**************************************************************************************************/
ROD0ScheduledUserAction::ROD0ScheduledUserAction(int deltaTimeMs) : ScheduledUserAction(deltaTimeMs)
/**************************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::constructor: Entered");

  m_deltaTimeMs = deltaTimeMs;
  m_low         = deltaTimeMs;
  m_hist        = new int[m_bins];
  m_errors      = new float[m_bins];
  for (int i = 0; i < m_bins; i++) 
  {
    m_hist[i] = 0;
    m_errors[i] = 0.01;
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::constructor: Done");
}


/*************************************************/
ROD0ScheduledUserAction::~ROD0ScheduledUserAction() 
/*************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::destructor: Entered");
  delete [] m_hist;
  delete [] m_errors;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::destructor: Done");
}


/*****************************************/
void ROD0ScheduledUserAction::reactTo(void)
/*****************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::reactTo entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0ScheduledUserAction::reactTo. Last time slice = " << getLastTimeSlice() << " ms");

  int lastTimeSlice = getLastTimeSlice();

  int low = m_low;
  for (int i = 1; i < (m_bins - 1); i++) 
  {
    if ((low <= lastTimeSlice) && (lastTimeSlice < (low + m_width))) 
      m_hist[i]++;

    low += m_width;
  }

  //overflow
  if (lastTimeSlice > low) 
    m_hist[m_bins - 1]++;

  //underflow
  if (lastTimeSlice < m_low)
    m_hist[0]++;
}


//Called from VMEReadoutModuleUser::publishFullStatistics
/****************************************************/
void ROD0ScheduledUserAction::printTimeHistogram(void)
/****************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::printTimeHistogram entered");

  std::cout << " Scheduler Time Distribution, DeltaTimeMs = " << m_deltaTimeMs << std::endl;
  for (int i = 0; i < (m_bins - 1); i++)
    std::cout << std::setw(10) << (m_low + i * m_width);

  std::cout << std::endl;

  for (int i = 1; i < m_bins; i++) 		// no underflow
    std::cout << std::setw(10) << m_hist[i];

  std::cout << std::endl;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::printTimeHistogram done");
}


//Called from VMEReadoutModuleUser::publishFullStatistics
/***************************************************************************/
void ROD0ScheduledUserAction::publishTimeHistogram(OHRawProvider<> *provider)
/***************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::publishTimeHistogram entered");

  provider->publish("VMEReadoutModuleUserTiming",
                    "Delta time between scheduled actions",
                    OHAxis("delta time", m_bins - 2, m_low, m_width), m_hist, m_errors, true);
}


//Called from VMEReadoutModuleUser::clearInfo
/*******************************************/
void ROD0ScheduledUserAction::clearInfo(void)
/*******************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::clearInfo entered");

  for (int i = 0; i < m_bins; i++) 
    m_hist[i] = 0;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::clearInfo done");
}


//Called from VMEReadoutModuleUser::prepareForRun
/***********************************************/
void ROD0ScheduledUserAction::prepareForRun(void)
/***********************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::prepareForRun entered");
  m_stopTriggerFlag = false;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::prepareForRun done");
}

//Called from VMEReadoutModuleUser::stopDC
/****************************************/
void ROD0ScheduledUserAction::stopDC(void)
/****************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::stopDC entered");
  m_stopTriggerFlag = true;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ScheduledUserAction::stopDC done");
}

