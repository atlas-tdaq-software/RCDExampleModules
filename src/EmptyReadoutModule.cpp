//$Id$
/************************************************************************/
/*									*/
/* File             : EmptyReadoutModule.cpp				*/
/*									*/
/* Authors          : S. Gameiro, CERN			                */
/*									*/
/* This file contains an empty ReadoutModule, for newcommers to         */
/* perform the RCD training or for RCD users in general. 	  	*/
/*									*/
/* For a ReadoutModule template containing more information in each	*/
/* method, please look at the MyReadoutModule.cpp file. For a complete 	*/
/* example on RCD look at the VMEReadoutModuleUser.cpp file. 		*/
/*									*/
/********* C 2005 - ROS/RCD  ********************************************/

#include <unistd.h>
#include <iostream>

#include "RCDExampleModules/EmptyReadoutModule.h"

// Includes the definition of tracing and debugging messages (DEBUG_TEXT, etc...)
#include "DFDebug/DFDebug.h"

#include "ROSUtilities/ROSErrorReporting.h"

// To handle the config exception in the "try-catch" instructions
#include "DFSubSystemItem/ConfigException.h"

// Variable to be used for tracing and debugging, for example: 

enum {
   DFDB_RCDDETECTOR = 30000	
  };

using namespace ROS;
using namespace RCD;

/******************************************************/
EmptyReadoutModule::EmptyReadoutModule()
/******************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::constructor: Entered");
  
  // Constructor: variable initialisation.
}

/*******************************************************/
EmptyReadoutModule::~EmptyReadoutModule()  noexcept
/*******************************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::destructor: Entered");
 
  // In the destructor you should delete the data channels that you have created, if you have some,
  // and also the various Handlers (Data interrupt handler, Error interrupt handler, user action scheduler)
}

/**************************************************************************************/
void EmptyReadoutModule::setup(DFCountedPointer<Config> configuration) 
/**************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::setup: Entered");
  
  // In this method you should extract the attributes and relationships from the configuration
  // object, which contains the parameters you defined in your schema.
}

/*****************************************************/
void EmptyReadoutModule::configure(const daq::rc::TransitionCmd&)
/*****************************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::configure: Entered");

  // In this method you should 
  // 	- establish all the harware connections (VME_Open(), TM_Open()...) and configure them (VME_MasterMap(),...)
  //	- create the Data Channels
  // 	- create all the necessary handlers: DataInterruptHandler, ErrorInterruptHandler, ScheduledUserAction.

  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::configure: Done");
}

/*****************************************************/
void EmptyReadoutModule::unconfigure(const daq::rc::TransitionCmd&)
/*****************************************************/
{
  
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::unconfigure: Entered");
  
  // Here you should "un-do" what you have done in the configure() method, except for the 
  // deletion of the various Handlers, which is done in the detructor:
  // 	- Closing the hardware connections
  // 	- Unconfiguring it (VME_MasterUnmap...)
  
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::unconfigure: Done");
}


/********************************************************/
void EmptyReadoutModule::connect(const daq::rc::TransitionCmd&)
/********************************************************/
{
 
  DEBUG_TEXT(DFDB_RCDDETECTOR, 20, "EmptyReadoutModule::connect: Entered");

  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::connect: Done");
}


/**********************************************************/
void EmptyReadoutModule::disconnect(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::disconnect: Entered");
  
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::disconnect: Done");
}
    

/************************************************************/    
void EmptyReadoutModule::prepareForRun(const daq::rc::TransitionCmd&)
/************************************************************/    
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::prepareForRun: Entered");
}


/*****************************************************/
void EmptyReadoutModule::stopDC(const daq::rc::TransitionCmd&)
/*****************************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::stopDC: Entered");
}


/******************************************************/
void EmptyReadoutModule::probe ()
/******************************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::probe: Entered");

// The probe() method will be called at a regular frequency (set in the ProbeInterval 
// attribute of the RunControlApplication class in the database), usually a high one.
// Tipically you would implement here the logic to publish to IS short status 
// information.

  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::probe: Done");
}

/******************************************************/
void EmptyReadoutModule::publishFullStatistics ()
/******************************************************/
{  
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::publishFullStatistics: Entered");
  
// The publishFullStatistics() method will be called at a regular frequency 
// (set in the FullStatisticsInterval attribute of the RunControlApplication 
// class in the database), usually a slow one.
// Tipically you would implement here the logic to publish big histograms that your
// module may produce.

  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::publishFullStatistics: Done");
}    

/*************************************************************************/
DFCountedPointer < Config > EmptyReadoutModule::getInfo()
/*************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::getInfo: Entered");

  DFCountedPointer<Config> configuration = Config::New();
  return(configuration);

}

/****************************************************/
void EmptyReadoutModule::clearInfo()
/****************************************************/
{
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "EmptyReadoutModule::clearInfo  Function entered");

}

extern "C" 
{
  extern ReadoutModule* createEmptyReadoutModule();
}
ReadoutModule* createEmptyReadoutModule()
{
  return (new EmptyReadoutModule());
}
