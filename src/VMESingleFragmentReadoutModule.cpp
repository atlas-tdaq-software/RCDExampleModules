//$Id$
/************************************************************************/
/*									*/
/* File             : VMESingleFragmentReadoutModule.cpp		*/
/*									*/
/* Authors          : G. Crone, B.Gorini,  J.Petersen ATD/CERN		*/
/*									*/
/********* C 2008 - ROS/RCD  ********************************************/

#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/ModulesException.h"
#include "RCDExampleModules/RCDModulesException.h"
#include "RCDExampleModules/VMESingleFragmentDataChannel.h"
#include "RCDExampleModules/VMESingleFragmentReadoutModule.h"


using namespace ROS;
using namespace RCD;


/**************************************************************/
VMESingleFragmentReadoutModule::VMESingleFragmentReadoutModule() 
/**************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::constructor: Entered (Empty function)");
}


/***************************************************************/
VMESingleFragmentReadoutModule::~VMESingleFragmentReadoutModule() noexcept
/***************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::destructor: Entered");

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		// we created them
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::destructor: Done");
}


// setup gets parameters from configuration
/********************************************************************************/
void VMESingleFragmentReadoutModule::setup(DFCountedPointer<Config> configuration) 
/********************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::setup: Entered");

  m_configuration = configuration;

  m_numberOfDataChannels = configuration->getInt("numberOfChannels");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMESingleFragmentReadoutModule::setup: numberOfDataChannels = " << m_numberOfDataChannels);

  // channel parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_id.push_back(0);
    m_rolPhysicalAddress.push_back(0);
  }

  // VMEbus parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_vmeAddress.push_back(0);
    m_vmeWindowSize.push_back(0);
    m_vmeAddressModifier.push_back(0);
    m_vmeScHandle.push_back(0);
    m_vmeVirtualPtr.push_back(0);
  }

  //get VMEbus parameters for the ROD MODULE
  u_int vmeModuleAddress         = configuration->getUInt("VMEbusAddress");
  u_int vmeModuleWindowSize      = configuration->getUInt("VMEbusWindowSize");
  u_int vmeModuleAddressModifier = configuration->getUInt("VMEbusAddressModifier");

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMESingleFragmentReadoutModule::setup: Module VMEbus parameters : "
             << "  vmeAddress = " << "0x" << HEX(vmeModuleAddress)
             << "  vmeWindowSize  = " << "0x" << HEX(vmeModuleWindowSize)
             << "  vmeAddressModifier = " << "0x" << HEX(vmeModuleAddressModifier));

  for (int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_id[chan]                 = configuration->getInt("channelId", chan);
    m_rolPhysicalAddress[chan] = configuration->getInt("ROLPhysicalAddress", chan);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMESingleFragmentReadoutModule::setup: chan = "
                                << chan << "  id = " << m_id[chan] << " rolPhysicalAddress = " << m_rolPhysicalAddress[chan]);

    m_vmeAddress[chan]         = vmeModuleAddress + m_rolPhysicalAddress[chan] * 0x800;  //This has to match VME_ROD_write
    m_vmeWindowSize[chan]      = vmeModuleWindowSize;
    m_vmeAddressModifier[chan] = vmeModuleAddressModifier;

    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: chan = " << chan
               << "  vmeAddress = " << "0x" << HEX(m_vmeAddress[chan])
               << "  vmeWindowSize  = " << "0x" << HEX(m_vmeWindowSize[chan])
               << "  vmeAddressModifier = " << "0x" << HEX(m_vmeAddressModifier[chan]));
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::setup: Done");
}


/**************************************************/
void VMESingleFragmentReadoutModule::configure(const daq::rc::TransitionCmd&)
/**************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::configure: Entered");

  err_type ret;
  err_str rcc_err_str;
  int scHandle;

  ret = VME_Open();
  if (ret != VME_SUCCESS) 
  {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleInterrupt::configure: Error from VME_Open");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_OPEN, rcc_err_str);  //Better use you own exception class
      throw ex1;
  }

  //Create the DataChannel objects
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    // VMEbus mapping
    VME_MasterMap_t master_map;
    master_map.vmebus_address   = m_vmeAddress[chan];
    master_map.window_size      = m_vmeWindowSize[chan];
    master_map.address_modifier = m_vmeAddressModifier[chan];
    master_map.options          = 0x0;          

    ret = VME_MasterMap(&master_map, &scHandle);
    if (ret != VME_SUCCESS) 
    {
        DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMESingleFragmentReadoutModule::configure: Error from VME_MasterMap");
        rcc_error_string(rcc_err_str, ret);
        CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_MASTERMAP, rcc_err_str);
        throw ex1;
    }
    m_vmeScHandle[chan] = scHandle;
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMESingleFragmentReadoutModule::configure: chan = " << chan << ", VME_MasterMap handle = " << scHandle);

    ret = VME_MasterMapVirtualLongAddress(scHandle, &m_vmeVirtualPtr[chan]);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::configure: chan = " << chan << ", VME_MasterMapVirtualAddress VA = " << HEX(m_vmeVirtualPtr[chan]));

    // chan == the Config Id
    VMESingleFragmentDataChannel *channel = new VMESingleFragmentDataChannel(m_id[chan], 
    									     chan, 
									     m_rolPhysicalAddress[chan],
									     m_vmeVirtualPtr[chan], 
									     m_configuration);
    m_dataChannels.push_back(channel);
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::configure: Done");
}


/**********************************************************************************/
void VMESingleFragmentReadoutModule::unconfigure(const daq::rc::TransitionCmd&)
/**********************************************************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::unconfigure: Entered ");

  // MasterUnmap the channels
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    ret = VME_MasterUnmap(m_vmeScHandle[chan]);
    if (ret != VME_SUCCESS) 
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMESingleFragmentReadoutModule::unconfigure: Error from VME_MasterUnmap");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_MASTERUNMAP, rcc_err_str);
      throw ex1;
    }

  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::unconfigure: VME_MasterUnmap done ");

  ret = VME_Close();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMESingleFragmentReadoutModule::unconfigure: Error from VME_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_CLOSE, rcc_err_str);
    throw ex1;
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::unconfigure: VME_close done ");

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::unconfigure: Exit ");

}


/************************************************/
void VMESingleFragmentReadoutModule::connect(const daq::rc::TransitionCmd&)
/************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::connect: Entered (empty function)");
}


/***************************************************/
void VMESingleFragmentReadoutModule::disconnect(const daq::rc::TransitionCmd&)
/***************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::disconnect: Entered (empty function)");
}
    

/******************************************************/    
void VMESingleFragmentReadoutModule::prepareForRun(const daq::rc::TransitionCmd&)
/******************************************************/    
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::prepareForRun: Entered (empty function)");
}


/***********************************************/
void VMESingleFragmentReadoutModule::stopDC(const daq::rc::TransitionCmd&)
/***********************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::stopDC: Entered (empty function)");
}


/**********************************************/
void VMESingleFragmentReadoutModule::clearInfo()
/**********************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentReadoutModule::clearInfo: Entered (empty function)");
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createVMESingleFragmentReadoutModule();
}
ReadoutModule* createVMESingleFragmentReadoutModule()
{
  return (new VMESingleFragmentReadoutModule());
}
