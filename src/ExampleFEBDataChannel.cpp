/*
  ATLAS RCD Software

  Class: ExampleFEBDataChannel
  Authors: ATLAS RCD group 	
*/

#include "RCDExampleModules/ExampleFEBDataChannel.h"
#include "ROSModules/ModulesException.h"
#include "ROSEventFragment/HWFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"
#include "rcc_time_stamp/tstamp.h"
#include "rcc_error/rcc_error.h"

using namespace RCD;
using namespace ROS;


/****************************************************************************/
ExampleFEBDataChannel::ExampleFEBDataChannel(u_int channelId,
					      u_int configId,
					      u_long channelPhysicalAddress,
					      DFCountedPointer<Config> config) 
  : DataChannel(channelId, configId, channelPhysicalAddress)
/****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "ExampleFEBDataChannel constructor\n");
 
  m_outinfo       = 0;
  m_memoryPool    = 0;
  m_mutex         = DFFastMutex::Create((char *) "REQUESTMUTEX");
  m_averageLength = 0;
  m_fragments     = 0;

  srand(1234567);

  DEBUG_TEXT(DFDB_ROSFM, 15, "ExampleFEBDataChannel created");  

  int npages   = config->getInt("memoryPageNumber");
  int pageSize = config->getInt("memoryPageSize");
  int pooltype = config->getInt("memoryPoolType");

  switch(pooltype)
  {
   case MemoryPool::ROSMEMORYPOOL_MALLOC:
     m_memoryPool = new MemoryPool_malloc(npages, pageSize);
     break;
   case MemoryPool::ROSMEMORYPOOL_CMEM_BPA:
   case MemoryPool::ROSMEMORYPOOL_CMEM_GFP:
     m_memoryPool = new MemoryPool_CMEM(npages, pageSize, pooltype);
     break;
   default:
     /* here add the appropriate exception */
     break;
  }

  m_maxFragmentSize = config->getInt  ("maxFragmentSize");
  m_fEmpty          = config->getFloat("fractionOfEmpty");
}


/*********************************************/
ExampleFEBDataChannel::~ExampleFEBDataChannel()
/*********************************************/
{
  m_mutex->destroy();
  delete m_memoryPool;
}


/*******************************************************/
DFCountedPointer<Config> ExampleFEBDataChannel::getInfo()
/*******************************************************/
{
  if(m_outinfo == 0)
    m_outinfo = Config::New();

  m_outinfo->set("NumberOfFragmentsServed", m_fragments);
  m_outinfo->set("AverageFragmentLength",   m_averageLength);

  return(m_outinfo);
}


/*************************************/
void ExampleFEBDataChannel::clearInfo()
/*************************************/
{
  m_fragments     = 0;
  m_averageLength = 0;
}


/**************************************************/
int ExampleFEBDataChannel::requestFragment(int nreq)
/**************************************************/
{
  float alea =((float) rand() /(float) RAND_MAX);
  return((alea > m_fEmpty) ? nreq : 0);
}


/***********************************************************/
EventFragment *ExampleFEBDataChannel::getFragment(int ticket)
/***********************************************************/
{
  if(! ticket)
    return 0;

  register float alea =((float) rand() /(float) RAND_MAX);
  register u_int length;

  while((length = (u_int)(alea *(float) m_maxFragmentSize) / sizeof(u_int)) == 0)
    alea =((float) rand() /(float) RAND_MAX);

  m_mutex->lock();  // protect unsafe allocation
  EventFragment *fragment = new HWFragment(m_memoryPool, length);
  m_mutex->unlock();

  m_averageLength *= m_fragments;
  m_averageLength +=(float) length;
  m_averageLength /=(float) ++m_fragments;

  return(fragment);
}
