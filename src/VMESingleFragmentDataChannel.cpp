//$Id$
/************************************************************************/
/*									*/
/* File             : VMESingleFragmentDataChannel.cpp			*/
/*									*/
/* Authors          : G. Crone, S. Gameiro, B.Gorini, J.Petersen CERN	*/
/*									*/
/********* C 2008 - ROS/RCD *********************************************/

#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFSubSystemItem/Config.h"
#include "RCDExampleModules/VMESingleFragmentDataChannel.h"


using namespace ROS;


/******************************************************************************************/
VMESingleFragmentDataChannel::VMESingleFragmentDataChannel(u_int channelId,
				   u_int channelIndex,
				   u_int rolPhysicalAddress,
				   u_long vmeBaseVA,
				   DFCountedPointer<Config> configuration,
				   VMESingleFragmentDataChannelInfo *info) :
  SingleFragmentDataChannel(channelId, channelIndex, rolPhysicalAddress, configuration,info)
/******************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentDataChannel::constructor: Entered");
  m_vmeBaseVA = vmeBaseVA;
  m_statistics = info;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentDataChannel::constructor: Done");
}


/***********************************************************/
VMESingleFragmentDataChannel::~VMESingleFragmentDataChannel() 
/***********************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentDataChannel::destructor: Entered (empty function)");
}


/******************************************************************/
int VMESingleFragmentDataChannel::getNextFragment(u_int *buffer,
						  int max_size,
						  u_int *status,
						  u_long pciAddress)
/******************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentDataChannel::getNextFragment: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMESingleFragmentDataChannel::getNextFragment: max_size = " << max_size);

  u_int *vmePtr = reinterpret_cast<u_int *>(m_vmeBaseVA);  // (virtual) base address of the channel
  u_int *bufPtr = buffer;                                  // the buffer address
  u_int fragmentSize = *vmePtr;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMESingleFragmentDataChannel::getNextFragment:fragment size = " << fragmentSize);

  vmePtr += 2;       // skip 2nd word  //MJ why?

  for (u_int i = 0; i < fragmentSize; i++) 
  {                  // read the ROD fragment in single cycle fast mode
    *bufPtr = *vmePtr;
    bufPtr++;
    vmePtr++;
  }

  *status = 0x5a5a;  //Just a pattern that can easily be spotted in the data file. Not actually an error

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "VMESingleFragmentDataChannel::getNextFragment: Done");
  return fragmentSize * sizeof(u_int);	// bytes ..
}


/***********************************************/
ISInfo *VMESingleFragmentDataChannel::getISInfo()
/***********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "VMESingleFragmentDataChannel::getIsInfo: called");
  DEBUG_TEXT(DFDB_ROSFM, 15, "VMESingleFragmentDataChannel::getIsInfo: done");
  return m_statistics;
}


/************************************************/
void VMESingleFragmentDataChannel::clearInfo(void)
/************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 5, "VMESingleFragmentDataChannel::clearInfo: Entered");
  SingleFragmentDataChannel::clearInfo();
  m_statistics->dummy = 0;
  DEBUG_TEXT(DFDB_ROSFM, 5, "VMESingleFragmentDataChannel::clearInfo: Done");
}

