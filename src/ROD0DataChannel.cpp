//$Id$
/************************************************************************/
/*									*/
/* File             : ROD0DataChannel.cpp				*/
/*									*/
/* Authors          : B.Gorini, M. Joos, J.Petersen CERN		*/
/*									*/
/********* C 2008 - ROS/RCD *********************************************/


#include <sys/types.h>
#include "rcc_corbo/rcc_corbo.h"
#include "rcc_corbo/rcc_corbo_int.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "RCDExampleModules/Corbo.h"
#include "RCDExampleModules/RCDModulesException.h"
#include "RCDExampleModules/ROD0DataChannel.h"


using namespace ROS;
using namespace RCD;


/***************************************************************************************/
ROD0DataChannel::ROD0DataChannel(u_int id,
				 u_int configId,
				 u_int rolPhysicalAddress,
			     	 bool usePolling,
				 DFCountedPointer<Config> configuration,
				 u_int vmeAddress,
				 u_long vmeBaseVA,
                                 VMEInterruptDescriptor vmeInterrupt, 
	                         int corboChannel,
				 bool blockTransferEnabled,
				 RODDataChannelInfo *info) : 
 SequentialDataChannel(id, configId, rolPhysicalAddress, usePolling, configuration, info)
/***************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::constructor: Entered");

  m_statistics           = info;
  m_vmeBaseVA            = vmeBaseVA;
  m_vmeAddress           = vmeAddress;
  m_interruptLevel       = vmeInterrupt.getLevel();
  m_usePolling           = usePolling;
  m_blockTransferEnabled = blockTransferEnabled;
  m_l1id_hack            = -1;
  m_corboChannel         = corboChannel;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::ROD0DataChannel: CORBO channel number = " << m_corboChannel);

  // clear from out to in, enable from in to out
  TM_csrW(m_corboChannel, DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);              // clear pending i/r
  TM_bimcrW(m_corboChannel, DIS_BIMCR0);

  // enable CORBO for data interrupts: write vector into CORBO
  if (!m_usePolling) 
  {
    TM_bimvrW(m_corboChannel, vmeInterrupt.getVector());
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ROD0DataChannel::ROD0DataChannel: CORBO vector = " << vmeInterrupt.getVector() << " loaded");
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::constructor: Done");
}


/*********************************/
ROD0DataChannel::~ROD0DataChannel() 
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::destructor: Entered (Empty function)");
}


// This function reads a ROD fragment from VME memory
// the VME memory has ROD fragments in each 2kByte page.
// Actually, each page starts with the size (in words) followed by a zero and then the ROD fragment
// the first fragment has L1ID = 0, the second L1ID = 1 ..... as produced by the utility VME_ROD_Write
// this is not so practical but we hack a bit to get around it ...
/**************************************************************************************/
int ROD0DataChannel::getNextFragment(u_int *buffer, int /*max_size*/, u_long pciAddress)
/**************************************************************************************/
{
  VME_BlockTransferList_t block_transfer_list;
  VME_ErrorCode_t status;
  int time_out = 1000;    /* time-out about 1 sec */
  VME_ErrorCode_t error_code;
  char error_string[VME_MAXSTRING];
	
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::getNextFragment: Entered");
  u_int *vmePtr = reinterpret_cast<u_int *>(m_vmeBaseVA);	// (virtual) base address of the channel
  u_int *bufPtr = buffer;	                                // the buffer address
  u_int fragmentSize = *vmePtr;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::getNextFragment: CORBO channel = " << m_corboChannel << ", fragment size = " << fragmentSize);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::getNextFragment: vmebus_address = " << HEX(m_vmeAddress));

  if (m_blockTransferEnabled) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::getNextFragment: block transfer: pci_address = " << HEX(pciAddress));
  
    block_transfer_list.list_of_items[0].vmebus_address       = m_vmeAddress + 8; // address of the beginning of the fragment
    block_transfer_list.list_of_items[0].system_iobus_address = pciAddress;
    block_transfer_list.list_of_items[0].size_requested       = fragmentSize * 4; // in bytes
    block_transfer_list.list_of_items[0].control_word         = VME_DMA_D64R;
    block_transfer_list.number_of_items                       = 1;

    if((error_code = VME_BlockTransfer(&block_transfer_list, time_out)))
    {
      if (error_code != VME_SUCCESS) 
      {
        VME_ErrorPrint(error_code);   					          // print error from VME_BlockTransfer
        if(!VME_BlockTransferStatus(&block_transfer_list, 0, &status)) 
	{	                                                                  // check status of block transfer 
	  VME_ErrorString(status, error_string);
          CREATE_ROS_EXCEPTION(ex1, RCDModulesException, BT_STATUS, error_string);
          throw(ex1);
        }
      }  
    }
  }
  else 
  { 
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::getNextFragment: single cycles");
    vmePtr += 2;					// skip 2nd word
    for (u_int i = 0; i < fragmentSize; i++) 
    {	                                                // read the ROD fragment in single cycle fast mode
     *bufPtr = *vmePtr;
     bufPtr++; 
     vmePtr++;
    }
  }
  
  // this is a "hack" to produce our own L1ID and BCID, since we are always reading the 1st fragment in the VMEbus memory.
  bufPtr = (u_int *)buffer;	// reset to beginning ..
  if (m_l1id_hack == -1) 
  {
    bufPtr[5] = 0;	        // replace with L1ID = 0
    bufPtr[6] = 0;	        // BCID = 0
    m_l1id_hack = 0;
  }
  else 
  {
    m_l1id_hack++;
    bufPtr[5] = m_l1id_hack;
    bufPtr[6] = m_l1id_hack;
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::getNextFragment: CORBO channel = " << m_corboChannel << ", bufPtr[5] = " << bufPtr[5]);

  // after the fragment has been read, the readout can be re-enabled
  if (m_usePolling) 
    TM_clbsy(m_corboChannel);
  else 
  { // interrupts
    TM_csrW(m_corboChannel, DIS_CSR1);          // disable (int) busy eqv trigger i/p
    TM_clbsy(m_corboChannel);                   // triggers may arrive, but disabled 

    if (m_stopTriggerFlag == false) 
    {		                                // only enable if trigger NOT stopped
      u_int ini_bimcr0 = INI_BIMCR0 + m_interruptLevel;
      TM_bimcrW(m_corboChannel, ini_bimcr0);    // reenable IE (BIM)
      TM_csrW(m_corboChannel, INI_CSR1);        // (re)enable triggers
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::getNextFragment: CORBO channel = " << m_corboChannel << " reenabled at level " << m_interruptLevel);
    }
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::getNextFragment: Done");
  return(fragmentSize * sizeof(u_int));	// bytes ..
}


/*****************************/
int ROD0DataChannel::poll(void)
/*****************************/
{
  int csrData;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::poll entered");

  TM_csrR(m_corboChannel, &csrData);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::poll: CORBO channel = " << m_corboChannel << " csrData = " << HEX(csrData));

  if (csrData & CORBO_CSR_BSSTATE) 
  { 	// local BUSY present
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::poll done (busy set)");
    return(1);
  }
  else 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataChannel::poll done (busy not set)");
    return 0;
  }
}


/***********************************/
void ROD0DataChannel::resetL1id(void)
/***********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::resetL1id entered");
  m_l1id_hack = -1;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::resetL1id done");
}


/***************************************/
void ROD0DataChannel::prepareForRun(void)
/***************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::prepareForRun entered");

  if (!m_usePolling) 
  {                                              // using interrupts
    TM_clbsy(m_corboChannel);                    // clear pending i/r
    u_int ini_bimcr0 = INI_BIMCR0 + m_interruptLevel;
    TM_bimcrW(m_corboChannel, ini_bimcr0);       // (re)enable IE (BIM)
  }
  TM_csrW(m_corboChannel, INI_CSR1);             // (re)enable triggers

  m_stopTriggerFlag = false;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::prepareForRun done");
}


/********************************/
void ROD0DataChannel::stopDC(void)
/********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::stopDC entered");

  m_stopTriggerFlag = true;

                                         // clear from out to in, enable from in to out
  TM_csrW(m_corboChannel, DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);              // clear pending i/r
  TM_bimcrW(m_corboChannel, DIS_BIMCR0);

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataChannel::stopDC done");
}


/**********************************/
ISInfo* ROD0DataChannel::getISInfo()
/**********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "ROD0DataChannel::getIsInfo: called and done");
  return m_statistics;
}


/***********************************/
void ROD0DataChannel::clearInfo(void)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 5, "ROD0DataChannel::clearInfo: Entered");
  SequentialDataChannel::clearInfo();
  m_statistics->dummy = 0;
  DEBUG_TEXT(DFDB_ROSFM, 5, "ROD0DataChannel::clearInfo: Done");
}

