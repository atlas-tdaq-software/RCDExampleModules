// $Id$
/************************************************************************/
/*									 */
/* File: VME_ROD_write.cpp						 */
/*									 */
/* This program creates either formatted or unformatted ROD fragments	 */
/* (complete ROD fragments with header and trailer or hardware fragments */
/* with only data) with input from the user, and writes them into 	 */
/* VMEbus memory.						         */
/*									 */
/* 27. oct. 04  Sonia Gameiro created					 */
/*									 */
/**************** C 2004 - A nickel program worth a dime *****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSEventFragment/RODFragment.h"
#include "ROSEventFragment/HWFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "DFDebug/DFDebug.h"
#include "DFThreads/DFFastMutex.h"

using namespace RCD;
using namespace ROS;

// Global variables
u_int physical_address = 0x08000000;
u_int window_size = 0x00800000;
u_int source_identifier = 0x500000;
u_int LVL1_trigger_type = 1;
u_int detector_event_type = 0xa;
u_int number_status_elements = 1;
u_int status_block_position = 1; 	// status elements are placed after the data
u_int max_fragment_size = 1024; 	// maximum fragment size counting header, data and trailer, in bytes
float fraction_empty_events = 0;
u_int run_number = 0;
u_int dblevel = 0; 
const u_int dbpackage = DFDB_ROSFM;
bool formatted = false;
bool randomSize = false;
const u_int number_pages = 4;  		// number of pages in the memory pool
int handle = 0;
MemoryPool *memory_pool;
bool special_pattern = false;		// if true, fills in the data inside the fragments with the pattern 
					// even word addresses: 0xaaaaaaaa
					// odd word addresses: 0x55555555
					// Used to test cross-talk between VME bus lines, and every bus line 
					// with 2 words only.

/***********/
void usage()
/***********/
{
  std::cout << std::endl;
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-a x: Physical address of the VME memory          ->Default: 0x" << std::hex << physical_address << std::endl;
  std::cout << "-w x: Window size of the VME memory               ->Default: 0x" << std::hex << window_size << std::endl;
  std::cout << "-f x: Maximum fragment size in bytes              ->Default: " << std::dec << max_fragment_size << std::endl;
  std::cout << "-R  : Random fragment size                        ->Default: " << std::dec << randomSize << std::endl;
  std::cout << "-e x: Fraction of empty events                    ->Default: " << std::dec << fraction_empty_events << std::endl;
  std::cout << "-b x: Debug level (0, 5, 10, 15, 20)              ->Default: " << std::dec << dblevel << std::endl;
  std::cout << "-i  : Special data pattern (odd address words 0xaaaaaaaa, even address words 0x55555555)." << std::endl;
  std::cout << "-F  : Formatted ROD fragments (header + data + trailer). Otherwise non formatted data." << std::endl;
  std::cout << "  Modifiers of -F option:" << std::endl;
  std::cout << "  -s x: Source identifier                         ->Default: " << std::dec << source_identifier << std::endl;
  std::cout << "  -t x: Level 1 trigger type                      ->Default: " << std::dec << LVL1_trigger_type << std::endl;
  std::cout << "  -d x: Detector event type                       ->Default: 0x" << std::hex << detector_event_type << std::endl;
  std::cout << "  -n x: Number of status elements                 ->Default: " << std::dec << number_status_elements << std::endl;
  std::cout << "  -p x: Status block position                     ->Default: " << std::dec << status_block_position << std::endl;
  std::cout << "  -r x: Run number                                ->Default: " << std::dec << run_number << std::endl;
  std::cout << std::endl;
}


/************/
void OpenVME()
/************/
{
  static VME_MasterMap_t master_map = {physical_address, window_size, VME_A32, 0};

  u_int ret = VME_Open();
  if (ret != VME_SUCCESS) 
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS) 
  {
     VME_ErrorPrint(ret);
     exit(-1);
  }
}


/*************/
void CloseVME()
/*************/
{
  u_int ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
}


/*********************************************/
HWFragment *createHWFragment(u_int *event_size)
/*********************************************/
{
  DEBUG_TEXT (DFDB_ROSFM, 15, "VME_ROD_write::createHWFragment: entered.");

  u_int length = 0;

  DFFastMutex *mutex = DFFastMutex::Create ((char *) "REQUESTMUTEX");

  float alea = ((float) rand () / (float) RAND_MAX);

  if (alea > fraction_empty_events)
  {
    while (length == 0)
    {
      u_int j;

      if (randomSize) 
      {
        alea = ((float) rand () / (float) RAND_MAX);
        j = (u_int) (alea * (float) (max_fragment_size));       // max_fragment_size is in bytes
        length = j - (j % sizeof (u_int));			// length is reduced to the next multiple of 4

        DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::createFragment: RODFragment data length in bytes is " << length);  
      }
      else 
      {
        j = max_fragment_size;  			        // max_fragment_size is in bytes
        length = j - (j % sizeof (u_int));		        // length is reduced to the next multiple of 4
      }
    }
  }

  mutex->lock();  						// protect unsafe allocation
  
  if (special_pattern) 
  {
    HWFragment *hwFragment = new HWFragment (memory_pool, length, HWFragment::ALTERNATE_BIT_PATTERN);
    mutex->unlock();
    *event_size = hwFragment->buffer()->size()/sizeof(u_int);  	// in words
    return (hwFragment);
  }
  else 
  {
    HWFragment *hwFragment = new HWFragment (memory_pool, length);
    mutex->unlock();
    *event_size = hwFragment->buffer()->size()/sizeof(u_int);  	// in words
    return (hwFragment);
  }
}


/***********************************************************/
RODFragment *createRODFragment(u_int l1id, u_int *event_size)
/***********************************************************/
{
  DEBUG_TEXT (DFDB_ROSFM, 15, "VME_ROD_write::createRODFragment: entered.");

  u_int length = 0;  
  u_int max_data_size = 0;

  DFFastMutex *mutex = DFFastMutex::Create ((char *) "REQUESTMUTEX");

  // Maximum ROS fragment data size is:
  //   header: 9 x 4 bytes = 36 bytes
  //   trailer: 3 x 4 bytes = 12 bytes
  //   Total (header + trailer + #status elements) = 48 bytes + #status elements
  //   max_data_size = (max_fragment_size - (48 + #status elements) bytes

  DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::createRODFragment: max_fragment_size is " << max_fragment_size);
  
  max_data_size = max_fragment_size - (48 + number_status_elements * sizeof (u_int));
  
  DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::createRODFragment: max_data_size is " << max_data_size);
  
  mutex->lock();  // protect unsafe allocation
  RODFragment *fragment = new RODFragment (memory_pool,
                                           source_identifier,
                                           l1id,
                                           l1id,
                                           LVL1_trigger_type,
                                           detector_event_type,
                                           number_status_elements,
                                           status_block_position,
					   run_number);
  mutex->unlock();
 
  DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::createRODFragment: created ROD fragment");
  
  float alea = ((float) rand () / (float) RAND_MAX);
  if (alea > fraction_empty_events)
  {
    while (length == 0)
    {
      u_int j;

      if (randomSize) 
      {
        alea = ((float) rand () / (float) RAND_MAX);
        j = (u_int) (alea * (float) (max_data_size));   // max_data_size is in bytes
        length = j - (j % sizeof (u_int));		// length is reduced to the next multiple of 4
      }
      else 
      {
        j = max_data_size;  			        // max_data_size is in bytes
        length = j - (j % sizeof (u_int));		// length is reduced to the next multiple of 4
      }
      DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::createRODFragment: RODFragment data length in bytes is " << length);  
    }

    if (special_pattern) 
    {
      mutex->lock();  						// protect unsafe allocation
      HWFragment *hwFragment = new HWFragment(memory_pool, length, HWFragment::ALTERNATE_BIT_PATTERN);
      mutex->unlock();
      fragment->append (hwFragment);
      mutex->lock();  			
      delete hwFragment;
      mutex->unlock();
    }
    else 
    {
      mutex->lock();  			
      HWFragment *hwFragment = new HWFragment(memory_pool, length);
      mutex->unlock();
      fragment->append (hwFragment);
      mutex->lock();  			
      delete hwFragment;
      mutex->unlock();
    }				
  }

  mutex->lock();
  fragment->appendTrailer(status_block_position, number_status_elements);
  mutex->unlock();

  *event_size = fragment->buffer()->size()/sizeof(u_int);  	// in words
  DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::createFragment: RODFragment total size in words = " << *event_size);
  return (fragment);
}


/****************/
int setdebug(void)
/****************/
{
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return 0;
}


/***************************************************************************/
void readoutVMEMemory (u_int max_number_events, u_int round_size, int handle)
/***************************************************************************/
{
  u_int size = 0, data = 0;

  std::cout << std::endl;
  std::cout << "Readout of the VME memory:" << std::endl; 
  std::cout << std::endl;
  
  for (u_int lvl1_id = 0; lvl1_id < max_number_events; lvl1_id++)
  { 
     u_int vmeptr = lvl1_id * round_size;
     u_int ret = VME_ReadSafeUInt(handle, vmeptr, &size);  // read the ROD/HW Fragment size in words
     if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);

     std::cout << " VMEbus address of fragment = " << std::hex << (physical_address + vmeptr) << std::endl;
     std::cout << " size of fragment  (words)  = " << std::dec << size << std::endl;
     std::cout << std::endl;

     DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::main: ROD/HW Fragment total size in words = " << std::dec << size);
     vmeptr += 8;
     for (u_int j = 0; j < size; j++)
     {
       ret = VME_ReadSafeUInt(handle, vmeptr, &data); 
       if (ret != VME_SUCCESS)
         VME_ErrorPrint(ret);
       std::cout << std::hex << data << std::dec << " ";
       vmeptr += 4;   	
     }	
     std::cout << std::endl;
     std::cout << std::endl;
  }  	
}


/************************/
void incompatibilityTest()
/************************/
{
  if (!formatted)
  {
    std::cout << "Options s, t, d, n, p and r can only be used together with the -F option" << std::endl;
    std::cout << std::endl;
    exit(0);
  }  
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  char c;
  u_int empty = 0;
  u_int event_size = 0;

  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "This program creates ROD fragments with input from the user" << std::endl;
  std::cout << "and writes them into VMEbus memory." << std::endl;
  std::cout << std::endl;

  while ((c = getopt(argc, argv,"ha:w:f:Rs:e:b:iFs:t:d:n:p:r:")) != -1)
    switch (c)
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
      usage();
      exit(0);
      break;

    case 'a': sscanf(optarg,"%x",&physical_address);     break;
    case 'w': sscanf(optarg,"%x",&window_size);		 break;
    case 'f': max_fragment_size = atoi(optarg);          break;	  // This includes everything, header, trailer, status elements...
    case 'R': randomSize = true;			 break;
    case 'e': fraction_empty_events = atof(optarg);      break;
    case 'b': dblevel = atoi(optarg);                    break;
    case 'i': special_pattern = true;			 break;
    case 'F': formatted = true;				 break;
    
    case 's': 
      source_identifier = atoi(optarg);
      incompatibilityTest();
      break;  
    
    case 't': 
      LVL1_trigger_type = atoi(optarg);
      incompatibilityTest();
      break;
      
    case 'd':
      sscanf(optarg,"%x",&detector_event_type);
      incompatibilityTest();
      break;
     
    case 'n':
      number_status_elements = atoi(optarg);
      incompatibilityTest();
      break;
      
    case 'p':
      status_block_position = atoi(optarg);
      incompatibilityTest();
      break;
      
    case 'r':
      run_number = atoi(optarg);
      incompatibilityTest();
      break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << "[options]" << std::endl;
      usage();
      exit (0);
    } 

  if (formatted && (max_fragment_size < (48 + number_status_elements * sizeof (u_int) + sizeof (u_int)))) 
  {
    std::cout << "-f option: minimum ROD fragment size is:" << std::endl;
    std::cout << "ROD header size + ROD trailer size + status elements size, plus one word." << std::endl;
    std::cout << "If using defaults, the minimum ROD fragment size is 56 bytes" << std::endl;
    std::cout << std::endl;
    exit(0);
  }
  
  if (!formatted && (max_fragment_size < 4)) 
  {
    std::cout << "-f option: minimum HW fragment size is 4 bytes" << std::endl;
    std::cout << std::endl;
    exit(0);
  }
  
  setdebug();    	

  OpenVME();

  srand(1234567);
  
  // Malloc memory pool, consisting in 4 pages with the maximum fragment size each
  memory_pool = new MemoryPool_malloc (number_pages, max_fragment_size); 
  DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::main: created memory pool");

  // number of RODFragments that fit in the VME memory is
  // window_size/max_fragment_size
  // but first rounding the fragment size to the next kbyte (block transfer purposes)
  // and adding 2 words, where we will place the size of the fragment
  // plus an empty one.
  // E.g. if max_fragment_size is 2350, round size is 3072 (3 kbytes)
  
  u_int round_size = (max_fragment_size + 2*sizeof(u_int) + 0x3ff) & 0xfffffc00;
  DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::main: Max round fragment size in bytes = " << round_size);

  u_int max_number_events = window_size / round_size;
  DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::main: Max number of events that fit into the VME memory = " << max_number_events);
   
  for (u_int lvl1_id = 0; lvl1_id < max_number_events; lvl1_id++)  
  {
    RODFragment *rod_fragment = 0;
    HWFragment *hw_fragment = 0;
     
    if (formatted)
      rod_fragment = createRODFragment(lvl1_id, &event_size);  //event_size is in words		
    else hw_fragment = createHWFragment(&event_size);
      
    u_int vmeptr = lvl1_id * round_size;      		  // ROS/HW Fragment space: 1 round size (max fragment size)
   
    u_int ret = VME_WriteSafeUInt(handle, vmeptr, event_size);  // first word of the ROD Fragment space is its lengh in words  
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
    
    ret = VME_WriteSafeUInt(handle, vmeptr += 4, empty);  // second word of the ROD Fragment space is zero 
    if (ret != VME_SUCCESS)				  // (for block transfer purposes)
      VME_ErrorPrint(ret);
   
    vmeptr += 4;					  // The ROD Fragment starts to be written in the 3rd word of its space.
 
    const Buffer *buffer = 0;
    
    if (formatted)
      buffer = rod_fragment->buffer();
    else buffer = hw_fragment->buffer();
    
    Buffer::page_iterator page; 

    for (page = buffer->begin(); page != buffer->end(); page++ ) 
    {
      u_int *ptr = (u_int *)(*page)->address();
	    
      for (u_int word = 0; word < (*page)->usedSize()/sizeof(u_int); word++) 
      {     
        DEBUG_TEXT (DFDB_ROSFM, 20, "VME_ROD_write::main: What we write in VME memory = " << std::hex << ptr[word]);
	ret = VME_WriteSafeUInt(handle, vmeptr, ptr[word]);   // writes them into VME memory
	if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);
	vmeptr += 4;
      } 
    }
    delete rod_fragment;
    delete hw_fragment;  
  }

  readoutVMEMemory(max_number_events, round_size, handle);		// prints the ROD Fragments to the screen
  CloseVME();
  delete memory_pool;
  return 0;
}
