//$Id$
/************************************************************************/
/*									*/
/* File             : ROD0ErrorInterruptHandler.cpp			*/
/*									*/
/* Authors          : B. Gorini, Markus Joos, J.Petersen CERN		*/
/*									*/
/********* C 2008 - ROS/RCD ******************************** ************/


#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_corbo/rcc_corbo.h"
#include "rcc_corbo/rcc_corbo_int.h"
#include "RCDExampleModules/Corbo.h"
#include "RCDExampleModules/RCDModulesException.h"
#include "RCDExampleModules/ROD0ErrorInterruptHandler.h"


using namespace RCD;
using namespace ROS;


/*****************************************************************************************/
ROD0ErrorInterruptHandler::ROD0ErrorInterruptHandler(VMEInterruptDescriptor vmeInterrupt) :
InterruptHandler(vmeInterrupt)
/*****************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::constructor: Entered");

  m_corboChannel   = 3;	// error uses Corbo channel 3 - hardwired ..  //There is a risk for a clach with a data channel!
  m_interruptLevel = vmeInterrupt.getLevel();

  // clear from out to in, enable from in to out
  TM_csrW(m_corboChannel, DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);               // clear pending i/r
  TM_bimcrW(m_corboChannel, DIS_BIMCR0);

  // enable CORBO for error interrupt: write vector into CORBO
  TM_bimvrW(m_corboChannel, vmeInterrupt.getVector());
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0ErrorInterruptHandler::constructor: CORBO error vector " << vmeInterrupt.getVector() << " loaded");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::constructor: Done");
}


/*******************************************************************************************************/
ROD0ErrorInterruptHandler::ROD0ErrorInterruptHandler(std::vector<VMEInterruptDescriptor> vmeInterrupts) :
InterruptHandler(vmeInterrupts)
/*******************************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::constructor: Entered (Empty function)");
}


/*****************************************************/
ROD0ErrorInterruptHandler::~ROD0ErrorInterruptHandler() 
/*****************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::destructor: Entered (Empty function)");
}


/**************************************************************************/
void ROD0ErrorInterruptHandler::reactTo(VMEInterruptDescriptor vmeInterrupt)
/**************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::reactTo entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0ErrorInterruptHandler::reactTo: vector = " << vmeInterrupt.getVector()
				  << " level = " << vmeInterrupt.getLevel() << " type = " << vmeInterrupt.getType());

  // produce error message ..
  CREATE_ROS_EXCEPTION(ex1, RCDModulesException, ERROR_INTERRUPT, "\n interrupt vector  = " << vmeInterrupt.getVector());
  ers::warning(ex1);

  // reenable the interrupt
  TM_csrW(m_corboChannel, DIS_CSR1);                // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);                         // triggers may arrive, but disabled

  if (m_stopTriggerFlag == false) 
  {                 // only enable if trigger NOT stopped
    u_int ini_bimcr0 = INI_BIMCR0 + m_interruptLevel;
    TM_bimcrW(m_corboChannel, ini_bimcr0);          // reenable IE (BIM)
    TM_csrW(m_corboChannel, INI_CSR1);              // (re)enable triggers
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0ErrorInterruptHandler::reactTo: CORBO channel = " << m_corboChannel << " reenabled at level " << m_interruptLevel);
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::reactTo done");
}


/*************************************************/
void ROD0ErrorInterruptHandler::prepareForRun(void)
/*************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::prepareForRun entered");

  TM_clbsy(m_corboChannel);                      // clear pending i/r
  u_int ini_bimcr0 = INI_BIMCR0 + m_interruptLevel;
  TM_bimcrW(m_corboChannel, ini_bimcr0);         // (re)enable IE (BIM)
  TM_csrW(m_corboChannel, INI_CSR1);             // (re)enable triggers

  m_stopTriggerFlag = false;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::prepareForRun done");
}


/******************************************/
void ROD0ErrorInterruptHandler::stopDC(void)
/******************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::stopDC entered");

  m_stopTriggerFlag = true;

  // clear from out to in, enable from in to out
  TM_csrW(m_corboChannel, DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);              // clear pending i/r
  TM_bimcrW(m_corboChannel, DIS_BIMCR0);

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0ErrorInterruptHandler::stopDC done");
}

