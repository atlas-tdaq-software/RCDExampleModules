//$Id$
/************************************************************************/
/*									*/
/* File             : ROD0DataInterruptHandler.cpp			*/
/*									*/
/* Authors          : B. Gorini, Markus Joos, J.Petersen CERN		*/
/*									*/
/********* C 2008 - ROS/RCD ******************************** ************/

#include "rcc_corbo/rcc_corbo.h"
#include "rcc_corbo/rcc_corbo_int.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "RCDExampleModules/ROD0DataInterruptHandler.h"


using namespace ROS;


/****************************************************************************************************************************/
ROD0DataInterruptHandler::ROD0DataInterruptHandler(VMEInterruptDescriptor vmeInterrupt, SequentialDataChannel * /*channel*/) :
InterruptHandler(vmeInterrupt)
/****************************************************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataInterruptHandler::constructor: Entered (Empty function)");
}


/***************************************************************************************************/
ROD0DataInterruptHandler::ROD0DataInterruptHandler(std::vector<VMEInterruptDescriptor> vmeInterrupts,
                                           std::vector<SequentialDataChannel *> channels) :
InterruptHandler(vmeInterrupts)
/***************************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataInterruptHandler::constructor: Entered");

  // build a map: vectors onto channels for this module
  for (u_int i = 0; i < vmeInterrupts.size(); i++) 
  {
    m_dataChannelsMap[vmeInterrupts[i].getVector()] = channels[i];
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataInterruptHandler::constructor: added vector " << vmeInterrupts[i].getVector() << " and channel " << channels[i] << " to map");
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataInterruptHandler::constructor: Done");
}


/***************************************************/
ROD0DataInterruptHandler::~ROD0DataInterruptHandler() 
/***************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ROD0DataInterruptHandler::destructor: Entered (Empty function)");
}

/*************************************************************************/
void ROD0DataInterruptHandler::reactTo(VMEInterruptDescriptor vmeInterrupt)
/*************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataInterruptHandler::reactTo entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataInterruptHandler::reactTo: vector = " << vmeInterrupt.getVector()
                                  << " level = " << vmeInterrupt.getLevel() << " type = " << vmeInterrupt.getType());


  if (m_dataChannelsMap.count(vmeInterrupt.getVector()) > 0) 
    m_dataChannelsMap[vmeInterrupt.getVector()]->signalFragmentAvailable();
  else 
  {
    // throw exception  //MJ: Add an exception!
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ROD0DataInterruptHandler::reactTo done");
}
