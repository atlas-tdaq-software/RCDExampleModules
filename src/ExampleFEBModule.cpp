/*
  ATLAS RCD Software

  Class: ExampleFEBModule
  Authors: ATLAS RCD group 	
*/

#include <iostream>
#include <unistd.h>
#include <stdlib.h>

#include "RCDExampleModules/ExampleFEBModule.h"
#include "RCDExampleModules/ExampleFEBDataChannel.h"
#include "ROSEventFragment/HWFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "DFSubSystemItem/Config.h"

using namespace RCD;
using namespace ROS;


/**********************************/
ExampleFEBModule::ExampleFEBModule()
/**********************************/
{
  m_modInfo         = 0;
  m_fEmpty          = 0;
  m_averageLength   = 0;
  m_fragments       = 0;
}


/***********************************/
ExampleFEBModule::~ExampleFEBModule() noexcept
/***********************************/
{
  int n = m_dataChannels.size();

  for(int i = 0; i < n; ++i)
    delete m_dataChannels[i];
}


/******************************************************************/
void ExampleFEBModule::setup(DFCountedPointer<Config> configuration)
/******************************************************************/
{
  // Get the number of data links for this module
  int nChannels = configuration->getInt("numberOfChannels");

  // Get link-specific parameters and create links
  // In general, reading VME modules for ROD emulation should be
  // done using one "RCDModule" for each link. This simplifies
  // The following procedure, is generic and works when controlling
  // sets of modules is needed for synchronization reasons.

  for(int i = 0; i < nChannels; ++i)
  {
    // contains DataChannel parameters
    DFCountedPointer<Config> dCConfig = Config::New();

    u_int channel_id        = configuration->getInt("channelId", i);
    u_long physical_address = configuration->getLUInt("ROLPhysicalAddress", i);
    int pageNumber          = configuration->getInt("memoryPoolNumPages", i);
    int pageSize            = configuration->getInt("memoryPoolPageSize", i);
    int poolType            = configuration->getInt("poolType", i);
    int maxsize             = configuration->getInt("maxFragmentSize");
    float fempty            = configuration->getFloat("fractionOfEmpty");

    dCConfig->set("memoryPageNumber", pageNumber);
    dCConfig->set("memoryPageSize",   pageSize);
    dCConfig->set("memoryPoolType",   poolType);
    dCConfig->set("maxFragmentSize",  maxsize);
    dCConfig->set("fractionOfEmpty",  fempty);

    DataChannel *channel = new ExampleFEBDataChannel(channel_id, i, physical_address, dCConfig);
    m_dataChannels.push_back(channel);
  }
}


/**************************************************/
DFCountedPointer<Config> ExampleFEBModule::getInfo()
/**************************************************/
{
  if(m_modInfo == 0)
    m_modInfo = Config::New();

  m_modInfo->set("NumberOfFragmentsServed",  m_fragments);
  m_modInfo->set("AverageFragmentLength",    m_averageLength);
  m_modInfo->set("FractionOfEmptyFragments", m_fEmpty);

  return m_modInfo;
}


/********************************/
void ExampleFEBModule::clearInfo()
/********************************/
{
  m_fEmpty        = 0;
  m_averageLength = 0;
  m_fragments     = 0;
}

extern "C" 
{
  extern ReadoutModule *createExampleFEBModule();
}

/*************************************/
ReadoutModule *createExampleFEBModule()
/*************************************/
{
  return(new ExampleFEBModule());
}
